f = open( "input/funcs.bin", "r" )
g = open( "method.dlist", "w" )


s = f.readline()
str_list = []
while ( s ):
    str_list.append ( s.split()[1][:-1] )
    s = f.readline()
    
st = set(str_list)

for i in st:
    g.write(i)
    g.write('\n')

f.close()
g.close()
