cd input
ren funcs*.bin funcs.bin
ren strs*.bin strs.bin
ren opcodes*.bin opcodes.bin
cd ..

python func_to_list.py
str_parser input\strs.bin str.txt
python unicode.py
transformer input\opcodes.bin tmp.bin
parser tmp.bin > opcodes.txt
python merge.py
del tmp.bin
del opcodes.txt
del str.txt
del str_gb.txt
del method.dlist