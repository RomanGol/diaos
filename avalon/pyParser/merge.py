f = open( "opcodes.txt", "r" )
g = open( "str_gb.txt", "r" )
h = open( "output/output.txt", "w" )

# read opcodes and make items for each opcodes
s = f.readline()
item = []
items = []
while ( s ):
    if s.find("uid") != -1:
        if len(item) > 0:
            items.append( item )
            item = []
            item.append(s)
        else:
            item.append(s)
    elif len(item) > 0:
        item.append(s)
    s = f.readline()

# read strings and append to certain item
s = g.readline()
while ( s ):
    n = int( s.split()[1] )
    content = g.readline()
    if ( n < len(items) ):
        items[n].append("|---------str: " + content)
    s = g.readline()

for it in items:
    for c in it:
        h.write(c)

f.close()
g.close()
h.close()
