﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;


namespace Andromeda.Android
{

    public class ReadFile
    {
        private readonly string _path;
        private Decoder _decoder;
        private Instruction _dei;
        private TracedInstruciton _tri;
        public ReadFile(string path)
        {
            _path = path;
        }
      
        static bool ReadStruct<T>(BinaryReader r, out T t)
        {
            int count = Marshal.SizeOf(typeof(T));
            var bytes = r.ReadBytes(count);
            if (bytes.Length < count)
            {
                t = default(T);
                return false;
            }
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            t = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
            return true;
        }
        public void Read()
        {
            using (var b = new BinaryReader(File.Open(_path, FileMode.Open)))
            {
                while (true)
                {
                    _decoder = new Decoder();                    
                    _dei = new Instruction();
                    if (!ReadStruct(b, out _tri)) break;
                    _dei = _decoder.Decode(_tri);
                    Console.WriteLine(_dei.Inst.Instruction.Opcode.ToString());
                    Console.WriteLine(_dei.Inst.Instruction.FormatType.ToString());
                    if(_dei.Inst.Instruction.Va.Used == true)
                    {
                        switch(_dei.Inst.Instruction.Va.Type)
                        {

                            case VarType.Reg4:
                                Console.WriteLine(string.Format("Va = V" + _dei.Inst.Instruction.Va.Num));
                                break;
                            case VarType.Reg8:
                                Console.WriteLine(string.Format("Va = V" + _dei.Inst.Instruction.Va.Num));
                                break;
                            case VarType.Reg16:
                                Console.WriteLine(string.Format("Va = V" +  _dei.Inst.Instruction.Va.Num));
                                break;
                            case VarType.Imm4:
                                Console.WriteLine(string.Format("ImmA = " + _dei.Inst.Instruction.Va.Num));
                                break;
                            case VarType.Imm8:
                                Console.WriteLine(string.Format("ImmA = " + _dei.Inst.Instruction.Va.Num));
                                break;
                            case VarType.Imm16:
                                Console.WriteLine(string.Format("ImmA = " + _dei.Inst.Instruction.Va.Num));
                                break;
                        }
                            
                    }
                    if (_dei.Inst.Instruction.Vb.Used == true)
                    {
                        switch (_dei.Inst.Instruction.Vb.Type)
                        {
                            case VarType.Reg4:
                                Console.WriteLine(string.Format("Vb = V" + _dei.Inst.Instruction.Vb.Num));
                                break;
                            case VarType.Reg8:
                                Console.WriteLine(string.Format("Vb = V" + _dei.Inst.Instruction.Vb.Num));
                                break;
                            case VarType.Reg16:
                                Console.WriteLine(string.Format("Vb = V" + _dei.Inst.Instruction.Vb.Num));
                                break;
                            case VarType.Imm4:
                                Console.WriteLine(string.Format("ImmB = " + _dei.Inst.Instruction.Vb.Num));
                                break;
                            case VarType.Imm8:
                                Console.WriteLine(string.Format("ImmB = " + _dei.Inst.Instruction.Vb.Num));
                                break;
                            case VarType.Imm16:
                                Console.WriteLine(string.Format("ImmB = " + _dei.Inst.Instruction.Vb.Num));
                                break;
                        }
                    }
                    if (_dei.Inst.Instruction.VbWide.Used == true)
                    {
                        Console.WriteLine("ImmBwide = V" + _dei.Inst.Instruction.VbWide.Num);
                    }
                    if (_dei.Inst.Instruction.Vc.Used == true)
                    {
                        switch (_dei.Inst.Instruction.Vc.Type)
                        {
                            case VarType.Reg4:
                                Console.WriteLine(string.Format("Vc = V" + _dei.Inst.Instruction.Vc.Num));
                                break;
                            case VarType.Reg8:
                                Console.WriteLine(string.Format("Vc = V" + _dei.Inst.Instruction.Vc.Num));
                                break;
                            case VarType.Reg16:
                                Console.WriteLine(string.Format("Vc = V" + _dei.Inst.Instruction.Vc.Num));
                                break;
                            case VarType.Imm4:
                                Console.WriteLine(string.Format("ImmC = " + _dei.Inst.Instruction.Vc.Num));
                                break;
                            case VarType.Imm8:
                                Console.WriteLine(string.Format("ImmC = " + _dei.Inst.Instruction.Vc.Num));
                                break;
                            case VarType.Imm16:
                                Console.WriteLine(string.Format("ImmC = " + _dei.Inst.Instruction.Vc.Num));
                                break;
                        }
                    }
                    Console.WriteLine("------------------------------------------");
                }
            }
        }
    }
}
