﻿
struct DecodedInstruction
{
    unsigned long ThreadId;
    unsigned long Pc;
    unsigned long Uid;
    unsigned char* Description;
    Instrtype Instruction;
}

struct Instrtype
{
    Opcode Opcode;
    InstructionFormat FormatType;
    InstructionIndexType IndexType;
    unsigned char Width;
    unsigned short * Inst;
       
    unsigned long MethodNameHash;
    unsigned long ClassNameHash;

    public Var32 Va;
    public Var32 Vb;
    public Var64 VbWide;/* for kFmt51l */
    public Var32 Vc;
    public Var32[] Args; /* vC/D/E/F/G in invoke or filled-new-array */
}

enum VarType
{
    Reg4,
    Reg8,
    Reg16,
    Imm4,
    Imm8,
    Imm16,
    Imm32,
    Imm64,
};

public struct Var32
{
    public VarType Type;
    public bool Signed;
    unsigned long Value;
    unsigned long Num;
    public bool Used;
    public Var32(int a)
    {
        Type = VarType.Reg8;
        Signed = false;
        Value = 0;
        Num = 0;
        Used = false;
    }
}

public struct Var64
{
    public VarType Type;
    public bool Signed;
    public Int32 Value;
    public UInt64 Num;
    public bool Used;
}
