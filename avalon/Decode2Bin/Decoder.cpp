﻿#include <cstdio>
#include "InstType.h"

struct RawInst
{
	u2 inst[5];
};

Inst disasm_inst( const RawInst & ri );

int main()
{
	FILE * f = fopen ( "opcodes.bin", "rb" );
	
	if ( f != NULL )
	{
		u4 pc;
		RawInst ri;
		while( !feof(f) )
		{
			fread ( &pc, sizeof(pc), 1, f );
			printf( "0x%08x\n", pc );
			fread ( &ri, sizeof(ri), 1, f );
			disasm_inst (ri);
		}
	}
	
	return 0;
}

	
Inst disasm_inst( const RawInst & ri )
{
	static Inst ins;
	u2 index = (ri.inst[0] & 0xff);
	if ( index == 0xff )
		index = 0x100 + (ri.inst[0] >> 8);
	
	ins.Opcode = (Opcode) index;
	ins.IndexType = InstIndexTypeTable[index];
	ins.Width = InstWidthTable[index];
	ins.FormatType = InstFormatTable[index];    	
	
    switch ( ins.FormatType )
    {
        case kFmt10x:       // op
            /* nothing to do; copy the AA bits out for the verifier */
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            break;
        case kFmt12x:       // op vA, vB
            ins.Va.Num = ((unsigned int) ((ri.inst[0] >> 8)& 0x0f));
            ins.Va.Type = Reg4;
            ins.Va.Used = true;
            ins.Vb.Num = ((unsigned int) (ri.inst[0] >> 12));
            ins.Vb.Type = Reg4;
            ins.Vb.Used = true;
            break;
        case kFmt11n:       // op vA, #+B
            ins.Va.Num = ((unsigned int) ((ri.inst[0] >> 8)& 0x0f));
            ins.Va.Type = Reg4;
            ins.Va.Used = true;
            ins.Vb.Num = (((unsigned int) (ri.inst[0] >> 12)) << 28) >> 28; // sign extend 4-bit value
            ins.Vb.Signed = true;
            ins.Vb.Type = Imm4;
            ins.Vb.Used = true;
            break;
        case kFmt11x:       // op vAA
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            break;
        case kFmt10t:       // op +AA
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));              // sign-extend 8-bit value
            ins.Va.Signed = true;
            ins.Va.Type = Imm8;
            ins.Va.Used = true;
            break;
        case kFmt20t:       // op +AAAA
            ins.Va.Num = ri.inst[1];                   // sign-extend 16-bit value
            ins.Va.Signed = true;
            ins.Va.Type = Imm16;
            ins.Va.Used = true;
            break;
        case kFmt20bc:      // [opt] op AA, thing@BBBB
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            ins.Vb.Num = ri.inst[1];
            ins.Vb.Type = Imm16;
            ins.Vb.Used = true;
            break;
        case kFmt21c:       // op vAA, thing@BBBB
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            ins.Vb.Num = ri.inst[1];
            ins.Vb.Type = Imm16;
            ins.Vb.Used = true;
            break;
        case kFmt22x:       // op vAA, vBBBB
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            ins.Vb.Num = ri.inst[1];
            ins.Vb.Type = Reg16;
            ins.Vb.Used = true;
            break;
        case kFmt21s:       // op vAA, #+BBBB
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            ins.Vb.Num = ri.inst[1];                   // sign-extend 16-bit value
            ins.Vb.Signed = true;
            ins.Vb.Type = Imm16;
            ins.Vb.Used = true;
            break;
        case kFmt21t:       // op vAA, +BBBB
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            ins.Vb.Num = ri.inst[1];                   // sign-extend 16-bit value
            ins.Vb.Signed = true;
            ins.Vb.Type = Imm16;
            ins.Vb.Used = true;
            break;
        case kFmt21h:       // op vAA, #+BBBB0000[00000000]
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            /*
             * The value should be treated as right-zero-extended, but we don't
             * actually do that here. Among other things, we don't know if it's
             * the top bits of a 32- or 64-bit value.
             */
            ins.Vb.Num = (unsigned int) (ri.inst[1] << 16);
            ins.Vb.Signed = true;
            ins.Vb.Used = true;
            break;
        case kFmt23x:       // op vAA, vBB, vCC
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            ins.Vb.Num = (unsigned int)(ri.inst[1] & 0xff);
            ins.Vb.Used = true;
            ins.Vc.Num = (unsigned int)(ri.inst[1] >> 8);
            ins.Vc.Used = true;
            break;
        case kFmt22b:       // op vAA, vBB, #+CC
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            ins.Vb.Num = (unsigned int)(ri.inst[1] & 0xff);
            ins.Vb.Used = true;
            ins.Vc.Num = (unsigned int)(ri.inst[1] >> 8);            // sign-extend 8-bit value
            ins.Vc.Signed = true;
            ins.Vc.Type = Imm8;
            ins.Vc.Used = true;
            break;
        case kFmt22s:       // op vA, vB, #+CCCC
            ins.Va.Num = ((unsigned int) ((ri.inst[0] >> 8)& 0x0f));
            ins.Va.Type = Reg4;
            ins.Va.Used = true;
            ins.Vb.Num = ((unsigned int) (ri.inst[0] >> 12));
            ins.Vb.Type = Reg4;
            ins.Vb.Used = true;
            ins.Vc.Num = ri.inst[1];                   // sign-extend 16-bit value
            ins.Vc.Signed = true;
            ins.Vc.Type = Imm16;
            ins.Vc.Used = true;
            break;
        case kFmt22t:       // op vA, vB, +CCCC
            ins.Va.Num = ((unsigned int) ((ri.inst[0] >> 8)& 0x0f));
            ins.Va.Type = Reg4;
            ins.Va.Used = true;
            ins.Vb.Num = ((unsigned int) (ri.inst[0] >> 12));
            ins.Vb.Type = Reg4;
            ins.Vb.Used = true;
            ins.Vc.Num = ri.inst[1];                   // sign-extend 16-bit value
            ins.Vc.Signed = true;
            ins.Vc.Type = Imm16;
            ins.Vc.Used = true;
            break;
        case kFmt22c:       // op vA, vB, thing@CCCC
            ins.Va.Num = ((unsigned int) ((ri.inst[0] >> 8)& 0x0f));
            ins.Va.Type = Reg4;
            ins.Va.Used = true;
            ins.Vb.Num = ((unsigned int) (ri.inst[0] >> 12));
            ins.Vb.Type = Reg4;
            ins.Vb.Used = true;
            ins.Vc.Num = ri.inst[1];
            ins.Vc.Type = Imm16;
            ins.Vc.Used = true;
            break;
        case kFmt22cs:      // [opt] op vA, vB, field offset CCCC
            ins.Va.Num = ((unsigned int) ((ri.inst[0] >> 8)& 0x0f));
            ins.Va.Type = Reg4;
            ins.Va.Used = true;
            ins.Vb.Num = ((unsigned int) (ri.inst[0] >> 12));
            ins.Vb.Type = Reg4;
            ins.Vb.Used = true;
            ins.Vc.Num = ri.inst[1];
            ins.Vc.Type = Imm16;
            ins.Vc.Used = true;
            break;
        case kFmt30t:       // op +AAAAAAAA
            ins.Va.Num = (ri.inst[1] | ((unsigned int)ri.inst[2] << 16));                     // signed 32-bit value
            ins.Va.Signed = true;
            ins.Va.Type = Imm32;
            ins.Va.Used = true;
            break;
        case kFmt31t:       // op vAA, +BBBBBBBB
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            ins.Vb.Num = (ri.inst[1] | ((unsigned int)ri.inst[2] << 16));                     // 32-bit value
            ins.Vb.Signed = true;
            ins.Vb.Type = Imm32;
            ins.Vb.Used = true;
            break;
        case kFmt31c:       // op vAA, string@BBBBBBBB
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            ins.Vb.Num = (ri.inst[1] | ((unsigned int)ri.inst[2] << 16));                     // 32-bit value
            ins.Vb.Type = Imm32;
            ins.Vb.Used = true;
            break;
        case kFmt32x:       // op vAAAA, vBBBB
            ins.Va.Num = ri.inst[1];
            ins.Va.Type = Reg16;
            ins.Va.Used = true;
            ins.Vb.Num = ri.inst[2];
            ins.Vb.Type =Reg16;
            ins.Vb.Used = true;
            break;
        case kFmt31i:       // op vAA, #+BBBBBBBB
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            ins.Vb.Num = (ri.inst[1] | ((unsigned int)ri.inst[2] << 16));                     // signed 32-bit value
            ins.Vb.Type = Imm32;
            ins.Vb.Signed = true;
            ins.Vb.Used = true;
            break;
        case kFmt35c:       // op {vC, vD, vE, vF, vG}, thing@BBBB
            {
                /*
                 * Note that the fields mentioned in the spec don't appear in
                 * their "usual" positions here compared to most formats. This
                 * was done so that the field names for the argument count and
                 * reference index match between this format and the corresponding
                 * range formats (3rc and friends).
                 *
                 * Bottom line: The argument count is always in vA, and the
                 * method constant (or equivalent) is always in vB.
                 */
                unsigned short regList;
                int i, count;

                ins.Va.Num = ((unsigned int) (ri.inst[0] >> 12)); // This is labeled A in the spec.
                ins.Va.Type = Imm4;
                ins.Va.Used = true;
                ins.Vb.Num = ri.inst[1];
                ins.Vb.Type = Imm16;
                ins.Vb.Used = true;
                regList = ri.inst[2];

                count =(int) ins.Va.Num;

                /*
                 * Copy the argument registers into the arg[] array, and
                 * also copy the first argument (if any) into vC. (The
                 * DecodedInstruction structure doesn't have separate
                 * fields for {vD, vE, vF, vG}, so there's no need to make
                 * copies of those.) Note that cases 5..2 fall through.
                 */
                switch (count)
                {
                    case 5:
                        {             
                            /*
                             * Per note at the top of this format decoder, the
                             * fifth argument comes from the A field in the
                             * instruction, but it's labeled G in the spec.
                             */
                            ins.Args[4].Num = ((unsigned int) ((ri.inst[0] >> 8)& 0x0f));
                            ins.Args[4].Type = Reg4;
                            ins.Args[4].Used = true;
                            ins.Args[3].Num = (unsigned int)((regList >> 12) & 0x0f);
                            ins.Args[3].Type = Reg4;
                            ins.Args[3].Used = true;
                            ins.Args[2].Num = (unsigned int)((regList >> 8) & 0x0f);
                            ins.Args[2].Type = Reg4;
                            ins.Args[2].Used = true;
                            ins.Args[1].Num = (unsigned int)((regList >> 4) & 0x0f);
                            ins.Args[1].Type = Reg4;
                            ins.Args[1].Used = true;
                            ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                        }
                    case 4: ins.Args[3].Num = (unsigned int)((regList >> 12) & 0x0f);
                            ins.Args[3].Type = Reg4;
                            ins.Args[3].Used = true;
                            ins.Args[2].Num = (unsigned int)((regList >> 8) & 0x0f);
                            ins.Args[2].Type = Reg4;
                            ins.Args[2].Used = true;
                            ins.Args[1].Num = (unsigned int)((regList >> 4) & 0x0f);
                            ins.Args[1].Type = Reg4;
                            ins.Args[1].Used = true;
                            ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                    case 3: ins.Args[2].Num = (unsigned int)((regList >> 8) & 0x0f);
                            ins.Args[2].Type = Reg4;
                            ins.Args[2].Used = true;
                            ins.Args[1].Num = (unsigned int)((regList >> 4) & 0x0f);
                            ins.Args[1].Type = Reg4;
                            ins.Args[1].Used = true;
                            ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                    case 2: ins.Args[1].Num = (unsigned int)((regList >> 4) & 0x0f);
                            ins.Args[1].Type = Reg4;
                            ins.Args[1].Used = true;
                            ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                    case 1: ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                    case 0: break; // Valid, but no need to do anything.
                    default:
                        //LOGW("Invalid arg count in 35c/35ms/35mi (%d)", count);
                        //goto bail;
                        break;
                }
            }
            break;
        case kFmt35ms:      // [opt] invoke-virtual+super
            {
                /*
                 * Note that the fields mentioned in the spec don't appear in
                 * their "usual" positions here compared to most formats. This
                 * was done so that the field names for the argument count and
                 * reference index match between this format and the corresponding
                 * range formats (3rc and friends).
                 *
                 * Bottom line: The argument count is always in vA, and the
                 * method constant (or equivalent) is always in vB.
                 */
                unsigned short regList;
                int i, count;

                ins.Va.Num = ((unsigned int)(ri.inst[0] >> 12)); // This is labeled A in the spec.
                ins.Va.Type = Imm4;
                ins.Va.Used = true;
                ins.Vb.Num = ri.inst[1];
                ins.Vb.Type = Imm16;
                ins.Vb.Used = true;
                regList = ri.inst[2];

                count =(int) ins.Va.Num;

                /*
                 * Copy the argument registers into the arg[] array, and
                 * also copy the first argument (if any) into vC. (The
                 * DecodedInstruction structure doesn't have separate
                 * fields for {vD, vE, vF, vG}, so there's no need to make
                 * copies of those.) Note that cases 5..2 fall through.
                 */
                switch (count)
                {
                    case 5:
                        {
                            /*
                             * Per note at the top of this format decoder, the
                             * fifth argument comes from the A field in the
                             * instruction, but it's labeled G in the spec.
                             */
                            ins.Args[4].Num = ((unsigned int)((ri.inst[0] >> 8) & 0x0f));
                            ins.Args[4].Type = Reg4;
                            ins.Args[4].Used = true;
                            ins.Args[3].Num = (unsigned int)((regList >> 12) & 0x0f);
                            ins.Args[3].Type = Reg4;
                            ins.Args[3].Used = true;
                            ins.Args[2].Num = (unsigned int)((regList >> 8) & 0x0f);
                            ins.Args[2].Type = Reg4;
                            ins.Args[2].Used = true;
                            ins.Args[1].Num = (unsigned int)((regList >> 4) & 0x0f);
                            ins.Args[1].Type = Reg4;
                            ins.Args[1].Used = true;
                            ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                        }
                    case 4: ins.Args[3].Num = (unsigned int)((regList >> 12) & 0x0f);
                            ins.Args[3].Type = Reg4;
                            ins.Args[3].Used = true;
                            ins.Args[2].Num = (unsigned int)((regList >> 8) & 0x0f);
                            ins.Args[2].Type = Reg4;
                            ins.Args[2].Used = true;
                            ins.Args[1].Num = (unsigned int)((regList >> 4) & 0x0f);
                            ins.Args[1].Type = Reg4;
                            ins.Args[1].Used = true;
                            ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                    case 3: ins.Args[2].Num = (unsigned int)((regList >> 8) & 0x0f);
                            ins.Args[2].Type = Reg4;
                            ins.Args[2].Used = true;
                            ins.Args[1].Num = (unsigned int)((regList >> 4) & 0x0f);
                            ins.Args[1].Type = Reg4;
                            ins.Args[1].Used = true;
                            ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                    case 2: ins.Args[1].Num = (unsigned int)((regList >> 4) & 0x0f);
                            ins.Args[1].Type = Reg4;
                            ins.Args[1].Used = true;
                            ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                    case 1: ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                    case 0: break; // Valid, but no need to do anything.
                    default:
                        //LOGW("Invalid arg count in 35c/35ms/35mi (%d)", count);
                        //goto bail;
                        break;
                }
            }
            break;
        case kFmt35mi:      // [opt] inline invoke
            {
                /*
                 * Note that the fields mentioned in the spec don't appear in
                 * their "usual" positions here compared to most formats. This
                 * was done so that the field names for the argument count and
                 * reference index match between this format and the corresponding
                 * range formats (3rc and friends).
                 *
                 * Bottom line: The argument count is always in vA, and the
                 * method constant (or equivalent) is always in vB.
                 */
                unsigned short regList;
                int i, count;

                ins.Va.Num = ((unsigned int)(ri.inst[0] >> 12)); // This is labeled A in the spec.
                ins.Va.Type = Imm4;
                ins.Va.Used = true;
                ins.Vb.Num = ri.inst[1];
                ins.Vb.Type = Imm16;
                ins.Vb.Used = true;
                regList = ri.inst[2];

                count =(int) ins.Va.Num;

                /*
                 * Copy the argument registers into the arg[] array, and
                 * also copy the first argument (if any) into vC. (The
                 * DecodedInstruction structure doesn't have separate
                 * fields for {vD, vE, vF, vG}, so there's no need to make
                 * copies of those.) Note that cases 5..2 fall through.
                 */
                switch (count)
                {
                    case 5:
                        {
                            /* A fifth arg is verboten for inline invokes. */
                            printf("Invalid arg count in 35mi (5)\n");
                            break;
                        }
                    case 4: ins.Args[3].Num = (unsigned int)((regList >> 12) & 0x0f);
                            ins.Args[3].Type = Reg4;
                            ins.Args[3].Used = true;
                            ins.Args[2].Num = (unsigned int)((regList >> 8) & 0x0f);
                            ins.Args[2].Type = Reg4;
                            ins.Args[2].Used = true;
                            ins.Args[1].Num = (unsigned int)((regList >> 4) & 0x0f);
                            ins.Args[1].Type = Reg4;
                            ins.Args[1].Used = true;
                            ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                    case 3: ins.Args[2].Num = (unsigned int)((regList >> 8) & 0x0f);
                            ins.Args[2].Type = Reg4;
                            ins.Args[2].Used = true;
                            ins.Args[1].Num = (unsigned int)((regList >> 4) & 0x0f);
                            ins.Args[1].Type = Reg4;
                            ins.Args[1].Used = true;
                            ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                    case 2: ins.Args[1].Num = (unsigned int)((regList >> 4) & 0x0f);
                            ins.Args[1].Type = Reg4;
                            ins.Args[1].Used = true;
                            ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                    case 1: ins.Vc.Num = ins.Args[0].Num = (unsigned int)(regList & 0x0f);
                            ins.Vc.Type = Reg4;
                            ins.Vc.Used = true;
                            ins.Args[0].Type = Reg4;
                            ins.Args[0].Used = true;
                            break;
                    case 0: break; // Valid, but no need to do anything.
                    default:
                        //LOGW("Invalid arg count in 35c/35ms/35mi (%d)", count);
                        //goto bail;
                        break;
                }
            }
            break;
        case kFmt3rc:       // op {vCCCC .. v(CCCC+AA-1)}, meth@BBBB
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Type = Imm8;
            ins.Va.Used = true;
            ins.Vb.Num = ri.inst[1];
            ins.Vb.Type =Imm16;
            ins.Vb.Used = true;
            ins.Vc.Num = ri.inst[2];
            ins.Vc.Type = Reg16;
            ins.Vc.Used = true;
            break;
        case kFmt3rms:      // [opt] invoke-virtual+super/range
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Type = Imm8;
            ins.Va.Used = true;
            ins.Vb.Num = ri.inst[1];
            ins.Vb.Type =Imm16;
            ins.Vb.Used = true;
            ins.Vc.Num = ri.inst[2];
            ins.Vc.Type = Reg16;
            ins.Vc.Used = true;
            break;
        case kFmt3rmi:      // [opt] execute-inline/range
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Type = Imm8;
            ins.Va.Used = true;
            ins.Vb.Num = ri.inst[1];
            ins.Vb.Type =Imm16;
            ins.Vb.Used = true;
            ins.Vc.Num = ri.inst[2];
            ins.Vc.Type = Reg16;
            ins.Vc.Used = true;
            break;
        case kFmt51l:       // op vAA, #+BBBBBBBBBBBBBBBB
            ins.Va.Num = ((unsigned int) (ri.inst[0] >> 8));
            ins.Va.Used = true;
            ins.VbWide.Num = (ri.inst[1] | ((unsigned int)ri.inst[2] << 16)) | ((u8)(ri.inst[3] | ((unsigned int)ri.inst[4] << 16)) << 32);
            ins.VbWide.Used = true;
            ins.VbWide.Type = Imm64;
            break;
        case kFmt33x:       // exop vAA, vBB, vCCCC
            ins.Va.Num = (unsigned int)(ri.inst[1] & 0xff);
            ins.Va.Used = true;
            ins.Vb.Num = (unsigned int)(ri.inst[1] >> 8);
            ins.Vb.Used = true;
            ins.Vc.Num = ri.inst[2];
            ins.Vc.Type = Reg16;
            ins.Vc.Used = true;
            break;
        case kFmt32s:       // exop vAA, vBB, #+CCCC
            ins.Va.Num = (unsigned int)ri.inst[1] & 0xff;
            ins.Va.Used = true;
            ins.Vb.Num = (unsigned int)ri.inst[1] >> 8;
            ins.Vb.Used = true;
            ins.Vc.Num = ri.inst[2];                   // sign-extend 16-bit value
            ins.Vc.Signed = true;
            ins.Vc.Type = Imm16;
            ins.Vc.Used = true;
            break;
        case kFmt40sc:      // [opt] exop AAAA, thing@BBBBBBBB
            /*
             * The order of fields for this format in the spec is {B, A},
             * to match formats 21c and 31c.
             */
            ins.Vb.Num = (ri.inst[1] | ((unsigned int)ri.inst[2] << 16));                     // 32-bit value
            ins.Vb.Type = Imm32;
            ins.Vb.Used = true;
            ins.Va.Num = ri.inst[3];
            ins.Va.Type = Imm16;
            ins.Va.Used = true;
            break;
        case kFmt41c:       // exop vAAAA, thing@BBBBBBBB
            /*
             * The order of fields for this format in the spec is {B, A},
             * to match formats 21c and 31c.
             */
            ins.Vb.Num = (ri.inst[1] | ((unsigned int)ri.inst[2] << 16));                     // 32-bit value
            ins.Vb.Type = Imm32;
            ins.Vb.Used = true;
            ins.Va.Num = ri.inst[3];
            ins.Va.Type = Reg16;
            ins.Va.Used = true;
            break;
        case kFmt52c:       // exop vAAAA, vBBBB, thing@CCCCCCCC
            /*
             * The order of fields for this format in the spec is {C, A, B},
             * to match formats 22c and 22cs.
             */
            ins.Vc.Num = (ri.inst[1] | ((unsigned int)ri.inst[2] << 16));                     // 32-bit value
            ins.Vc.Type = Imm32;
            ins.Vc.Used = true;
            ins.Va.Num = ri.inst[3];
            ins.Va.Type = Reg16;
            ins.Va.Used = true;
            ins.Vb.Num = ri.inst[4];
            ins.Vb.Type = Reg16;
            ins.Vb.Used = true;
            break;
        case kFmt5rc:       // exop {vCCCC .. v(CCCC+AAAA-1)}, meth@BBBBBBBB
            /*
             * The order of fields for this format in the spec is {B, A, C},
             * to match formats 3rc and friends.
             */
            ins.Vb.Num = (ri.inst[1] | ((unsigned int) ri.inst[2] << 16));                     // 32-bit value
            ins.Vb.Type = Imm32;
            ins.Vb.Used = true;
            ins.Va.Num = ri.inst[3];
            ins.Va.Type =Imm16;
            ins.Va.Used = true;
            ins.Vc.Num = ri.inst[4];
            ins.Vc.Type = Reg16;
            ins.Vc.Used = true;
            break;
        default:
            printf ( "Can't decode unexpected format %d (op=%d)\n", ins.FormatType, ins.Opcode );
            break;
    }
    
    return ins;
}
