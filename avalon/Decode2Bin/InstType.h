﻿#ifndef _INST_TYPE_H
#define _INST_TYPE_H	

#include "DecoderTable.h"

static const u2 InstMaxNum = 5;
static const u2 MethodNameMaxLen = 256;
static const u2 ClassNameMaxLen = 256;

enum VarType
{
    Reg4,
    Reg8,
    Reg16,
    Imm4,
    Imm8,
    Imm16,
    Imm32,
    Imm64,
};

/*
struct TracedInstruciton
{
    unsigned long ThreadId;
    unsigned long PC;
    unsigned long ClassNameHash;
    unsigned long MethodNameHash;
    unsigned long UID;
    Buff Buffer;
    unsigned short Inst[];
};

struct Buff
{
    unsigned char Index[];
    unsigned long Buf[];
};
struct DecodedInstruction
{
    unsigned long ThreadId;
    unsigned long Pc;
    unsigned long Uid;
    unsigned char* Description;
    Instrtype Instruction;
}


*/

struct Instrtype
{
	s2 category; //类型
	u2 length; //长度,1~5	 
	u2 inst[InstMaxNum];
	char inst_description[32];
	char method_name[MethodNameMaxLen];
	char class_name[ClassNameMaxLen];
};




struct Var32
{
    VarType Type;
    bool Signed;
    unsigned long Value;
    unsigned long Num;
    bool Used;
};


struct Var64
{
    VarType Type;
    bool Signed;
    u4 Value;
    u8 Num;
    bool Used;
};


struct Inst
{
    Opcode Opcode;
    InstFormat FormatType;
    InstIndexType IndexType;
    u1 Width;
    Var32 Va;
    Var32 Vb;
    Var64 VbWide;/* for kFmt51l */
    Var32 Vc;
    Var32 Args[256]; /* vC/D/E/F/G in invoke or filled-new-array */
};

#endif
