#ifndef _METHOD_H_
#define _METHOD_H_

#include "trace.h"

void ldvm_OP_NOP(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE_FROM16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE_16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_ADD_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CONST_4(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CONST_16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CONST(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CONST_HIGH16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE_WIDE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE_WIDE_FROM16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE_WIDE_16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE_OBJECT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE_OBJECT_FROM16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE_OBJECT_16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE_RESULT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE_RESULT_WIDE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE_RESULT_OBJECT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MOVE_EXCEPTION(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_RETURN_VOID(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_RETURN(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_RETURN_WIDE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_RETURN_OBJECT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CONST_WIDE_16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CONST_WIDE_32(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CONST_WIDE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CONST_WIDE_HIGH16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CONST_STRING(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CONST_STRING_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CONST_CLASS(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MONITOR_ENTER(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MONITOR_EXIT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CHECK_CAST(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INSTANCE_OF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_ARRAY_LENGTH(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_NEW_INSTANCE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_NEW_ARRAY(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_FILLED_NEW_ARRAY(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_FILLED_NEW_ARRAY_RANGE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_FILL_ARRAY_DATA(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_THROW(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_GOTO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_GOTO_16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_GOTO_32(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_PACKED_SWITCH(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPARSE_SWITCH(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CMPL_FLOAT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CMPG_FLOAT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CMPL_DOUBLE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CMPG_DOUBLE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CMP_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IF_EQ(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IF_NE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IF_GE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IF_LT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IF_GT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IF_LE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IF_EQZ(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IF_NEZ(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IF_LTZ(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IF_GEZ(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IF_GTZ(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IF_LEZ(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_3E(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_3F(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_40(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_41(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_42(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_43(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AGET(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AGET_WIDE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AGET_OBJECT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AGET_BOOLEAN(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AGET_BYTE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AGET_CHAR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AGET_SHORT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_APUT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_APUT_WIDE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_APUT_OBJECT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_APUT_BOOLEAN(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_APUT_BYTE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_APUT_CHAR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_APUT_SHORT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_WIDE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_OBJECT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_BOOLEAN(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_BYTE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_CHAR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_SHORT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_WIDE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_OBJECT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_BOOLEAN(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_BYTE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_CHAR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_SHORT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_WIDE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_OBJECT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_BOOLEAN(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_BYTE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_CHAR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_SHORT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_WIDE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_OBJECT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_BOOLEAN(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_BYTE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_CHAR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_SHORT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_VIRTUAL(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_SUPER(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_DIRECT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_STATIC(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_INTERFACE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_73(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_VIRTUAL_RANGE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_SUPER_RANGE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_DIRECT_RANGE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_STATIC_RANGE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_INTERFACE_RANGE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_79(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_7A(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_NEG_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_NOT_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_NEG_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_NOT_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_NEG_FLOAT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_NEG_DOUBLE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INT_TO_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INT_TO_FLOAT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INT_TO_DOUBLE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_LONG_TO_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_LONG_TO_FLOAT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_LONG_TO_DOUBLE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_FLOAT_TO_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_FLOAT_TO_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_FLOAT_TO_DOUBLE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DOUBLE_TO_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DOUBLE_TO_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DOUBLE_TO_FLOAT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INT_TO_BYTE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INT_TO_CHAR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INT_TO_SHORT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SUB_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MUL_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DIV_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_REM_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AND_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_OR_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_XOR_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SHL_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SHR_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_USHR_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_ADD_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SUB_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MUL_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DIV_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_REM_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AND_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_OR_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_XOR_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SHL_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SHR_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_USHR_LONG(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_ADD_FLOAT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SUB_FLOAT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MUL_FLOAT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DIV_FLOAT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_REM_FLOAT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_ADD_DOUBLE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SUB_DOUBLE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MUL_DOUBLE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DIV_DOUBLE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_REM_DOUBLE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_ADD_INT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SUB_INT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MUL_INT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DIV_INT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_REM_INT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AND_INT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_OR_INT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_XOR_INT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SHL_INT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SHR_INT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_USHR_INT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_ADD_LONG_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SUB_LONG_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MUL_LONG_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DIV_LONG_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_REM_LONG_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AND_LONG_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_OR_LONG_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_XOR_LONG_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SHL_LONG_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SHR_LONG_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_USHR_LONG_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_ADD_FLOAT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SUB_FLOAT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MUL_FLOAT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DIV_FLOAT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_REM_FLOAT_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_ADD_DOUBLE_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SUB_DOUBLE_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MUL_DOUBLE_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DIV_DOUBLE_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_REM_DOUBLE_2ADDR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_ADD_INT_LIT16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_RSUB_INT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MUL_INT_LIT16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DIV_INT_LIT16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_REM_INT_LIT16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AND_INT_LIT16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_OR_INT_LIT16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_XOR_INT_LIT16(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_ADD_INT_LIT8(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_RSUB_INT_LIT8(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_MUL_INT_LIT8(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DIV_INT_LIT8(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_REM_INT_LIT8(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_AND_INT_LIT8(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_OR_INT_LIT8(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_XOR_INT_LIT8(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SHL_INT_LIT8(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SHR_INT_LIT8(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_USHR_INT_LIT8(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_VOLATILE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_VOLATILE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_VOLATILE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_VOLATILE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_OBJECT_VOLATILE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_WIDE_VOLATILE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_WIDE_VOLATILE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_WIDE_VOLATILE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_WIDE_VOLATILE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_BREAKPOINT(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_THROW_VERIFICATION_ERROR(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_EXECUTE_INLINE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_EXECUTE_INLINE_RANGE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_OBJECT_INIT_RANGE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_RETURN_VOID_BARRIER(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_QUICK(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_WIDE_QUICK(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_OBJECT_QUICK(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_QUICK(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_WIDE_QUICK(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_OBJECT_QUICK(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_VIRTUAL_QUICK(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_VIRTUAL_QUICK_RANGE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_SUPER_QUICK(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_SUPER_QUICK_RANGE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_OBJECT_VOLATILE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_OBJECT_VOLATILE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_OBJECT_VOLATILE(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_DISPATCH_FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CONST_CLASS_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_CHECK_CAST_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INSTANCE_OF_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_NEW_INSTANCE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_NEW_ARRAY_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_FILLED_NEW_ARRAY_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_WIDE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_OBJECT_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_BOOLEAN_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_BYTE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_CHAR_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_SHORT_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_WIDE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_OBJECT_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_BOOLEAN_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_BYTE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_CHAR_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_SHORT_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_WIDE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_OBJECT_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_BOOLEAN_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_BYTE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_CHAR_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_SHORT_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_WIDE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_OBJECT_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_BOOLEAN_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_BYTE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_CHAR_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_SHORT_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_VIRTUAL_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_SUPER_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_DIRECT_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_STATIC_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_INTERFACE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_27FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_28FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_29FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_2AFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_2BFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_2CFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_2DFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_2EFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_2FFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_30FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_31FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_32FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_33FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_34FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_35FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_36FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_37FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_38FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_39FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_3AFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_3BFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_3CFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_3DFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_3EFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_3FFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_40FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_41FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_42FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_43FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_44FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_45FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_46FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_47FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_48FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_49FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_4AFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_4BFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_4CFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_4DFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_4EFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_4FFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_50FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_51FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_52FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_53FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_54FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_55FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_56FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_57FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_58FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_59FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_5AFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_5BFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_5CFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_5DFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_5EFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_5FFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_60FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_61FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_62FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_63FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_64FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_65FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_66FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_67FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_68FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_69FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_6AFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_6BFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_6CFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_6DFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_6EFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_6FFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_70FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_71FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_72FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_73FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_74FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_75FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_76FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_77FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_78FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_79FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_7AFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_7BFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_7CFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_7DFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_7EFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_7FFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_80FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_81FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_82FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_83FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_84FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_85FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_86FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_87FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_88FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_89FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_8AFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_8BFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_8CFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_8DFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_8EFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_8FFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_90FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_91FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_92FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_93FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_94FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_95FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_96FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_97FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_98FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_99FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_9AFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_9BFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_9CFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_9DFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_9EFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_9FFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_A0FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_A1FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_A2FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_A3FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_A4FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_A5FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_A6FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_A7FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_A8FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_A9FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_AAFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_ABFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_ACFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_ADFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_AEFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_AFFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_B0FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_B1FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_B2FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_B3FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_B4FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_B5FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_B6FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_B7FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_B8FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_B9FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_BAFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_BBFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_BCFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_BDFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_BEFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_BFFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_C0FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_C1FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_C2FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_C3FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_C4FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_C5FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_C6FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_C7FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_C8FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_C9FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_CAFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_CBFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_CCFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_CDFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_CEFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_CFFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_D0FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_D1FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_D2FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_D3FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_D4FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_D5FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_D6FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_D7FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_D8FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_D9FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_DAFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_DBFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_DCFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_DDFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_DEFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_DFFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_E0FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_E1FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_E2FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_E3FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_E4FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_E5FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_E6FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_E7FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_E8FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_E9FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_EAFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_EBFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_ECFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_EDFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_EEFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_EFFF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_F0FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_UNUSED_F1FF(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_INVOKE_OBJECT_INIT_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_VOLATILE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_WIDE_VOLATILE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IGET_OBJECT_VOLATILE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_VOLATILE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_WIDE_VOLATILE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_IPUT_OBJECT_VOLATILE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_VOLATILE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_WIDE_VOLATILE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SGET_OBJECT_VOLATILE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_VOLATILE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_WIDE_VOLATILE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_SPUT_OBJECT_VOLATILE_JUMBO(const Loccs_opcode lo ,FILE* fpw);
void ldvm_OP_THROW_VERIFICATION_ERROR_JUMBO(const Loccs_opcode lo ,FILE* fpw);

static void (*opcodeHandlers[0x200])(const Loccs_opcode lo ,FILE* fpw) = 
{
    /* BEGIN(libdex-goto-table); GENERATED AUTOMATICALLY BY opcode-gen */
    ldvm_OP_NOP,                                  
    ldvm_OP_MOVE,                                 
    ldvm_OP_MOVE_FROM16,                          
    ldvm_OP_MOVE_16,                              
    ldvm_OP_MOVE_WIDE,                            
    ldvm_OP_MOVE_WIDE_FROM16,                     
    ldvm_OP_MOVE_WIDE_16,                         
    ldvm_OP_MOVE_OBJECT,                          
    ldvm_OP_MOVE_OBJECT_FROM16,                   
    ldvm_OP_MOVE_OBJECT_16,                       
    ldvm_OP_MOVE_RESULT,                          
    ldvm_OP_MOVE_RESULT_WIDE,                     
    ldvm_OP_MOVE_RESULT_OBJECT,                   
    ldvm_OP_MOVE_EXCEPTION,                       
    ldvm_OP_RETURN_VOID,                          
    ldvm_OP_RETURN,                               
    ldvm_OP_RETURN_WIDE,                          
    ldvm_OP_RETURN_OBJECT,                        
    ldvm_OP_CONST_4,                              
    ldvm_OP_CONST_16,                             
    ldvm_OP_CONST,                                
    ldvm_OP_CONST_HIGH16,                         
    ldvm_OP_CONST_WIDE_16,                        
    ldvm_OP_CONST_WIDE_32,                        
    ldvm_OP_CONST_WIDE,                           
    ldvm_OP_CONST_WIDE_HIGH16,                    
    ldvm_OP_CONST_STRING,                         
    ldvm_OP_CONST_STRING_JUMBO,                   
    ldvm_OP_CONST_CLASS,                          
    ldvm_OP_MONITOR_ENTER,                        
    ldvm_OP_MONITOR_EXIT,                         
    ldvm_OP_CHECK_CAST,                           
    ldvm_OP_INSTANCE_OF,                          
    ldvm_OP_ARRAY_LENGTH,                         
    ldvm_OP_NEW_INSTANCE,                         
    ldvm_OP_NEW_ARRAY,                            
    ldvm_OP_FILLED_NEW_ARRAY,                     
    ldvm_OP_FILLED_NEW_ARRAY_RANGE,               
    ldvm_OP_FILL_ARRAY_DATA,                      
    ldvm_OP_THROW,                                
    ldvm_OP_GOTO,                                 
    ldvm_OP_GOTO_16,                              
    ldvm_OP_GOTO_32,                              
    ldvm_OP_PACKED_SWITCH,                        
    ldvm_OP_SPARSE_SWITCH,                        
    ldvm_OP_CMPL_FLOAT,                           
    ldvm_OP_CMPG_FLOAT,                           
    ldvm_OP_CMPL_DOUBLE,                          
    ldvm_OP_CMPG_DOUBLE,                          
    ldvm_OP_CMP_LONG,                             
    ldvm_OP_IF_EQ,                                
    ldvm_OP_IF_NE,                                
    ldvm_OP_IF_LT,                                
    ldvm_OP_IF_GE,                                
    ldvm_OP_IF_GT,                                
    ldvm_OP_IF_LE,                                
    ldvm_OP_IF_EQZ,                               
    ldvm_OP_IF_NEZ,                               
    ldvm_OP_IF_LTZ,                               
    ldvm_OP_IF_GEZ,                               
    ldvm_OP_IF_GTZ,                               
    ldvm_OP_IF_LEZ,                               
    ldvm_OP_UNUSED_3E,                            
    ldvm_OP_UNUSED_3F,                            
    ldvm_OP_UNUSED_40,                            
    ldvm_OP_UNUSED_41,                            
    ldvm_OP_UNUSED_42,                            
    ldvm_OP_UNUSED_43,                            
    ldvm_OP_AGET,                                 
    ldvm_OP_AGET_WIDE,                            
    ldvm_OP_AGET_OBJECT,                          
    ldvm_OP_AGET_BOOLEAN,                         
    ldvm_OP_AGET_BYTE,                            
    ldvm_OP_AGET_CHAR,                            
    ldvm_OP_AGET_SHORT,                           
    ldvm_OP_APUT,                                 
    ldvm_OP_APUT_WIDE,                            
    ldvm_OP_APUT_OBJECT,                          
    ldvm_OP_APUT_BOOLEAN,                         
    ldvm_OP_APUT_BYTE,                            
    ldvm_OP_APUT_CHAR,                            
    ldvm_OP_APUT_SHORT,                           
    ldvm_OP_IGET,                                 
    ldvm_OP_IGET_WIDE,                            
    ldvm_OP_IGET_OBJECT,                          
    ldvm_OP_IGET_BOOLEAN,                         
    ldvm_OP_IGET_BYTE,                            
    ldvm_OP_IGET_CHAR,                            
    ldvm_OP_IGET_SHORT,                           
    ldvm_OP_IPUT,                                 
    ldvm_OP_IPUT_WIDE,                            
    ldvm_OP_IPUT_OBJECT,                          
    ldvm_OP_IPUT_BOOLEAN,                         
    ldvm_OP_IPUT_BYTE,                            
    ldvm_OP_IPUT_CHAR,                            
    ldvm_OP_IPUT_SHORT,                           
    ldvm_OP_SGET,                                 
    ldvm_OP_SGET_WIDE,                            
    ldvm_OP_SGET_OBJECT,                          
    ldvm_OP_SGET_BOOLEAN,                         
    ldvm_OP_SGET_BYTE,                            
    ldvm_OP_SGET_CHAR,                            
    ldvm_OP_SGET_SHORT,                           
    ldvm_OP_SPUT,                                 
    ldvm_OP_SPUT_WIDE,                            
    ldvm_OP_SPUT_OBJECT,                          
    ldvm_OP_SPUT_BOOLEAN,                         
    ldvm_OP_SPUT_BYTE,                            
    ldvm_OP_SPUT_CHAR,                            
    ldvm_OP_SPUT_SHORT,                           
    ldvm_OP_INVOKE_VIRTUAL,                       
    ldvm_OP_INVOKE_SUPER,                         
    ldvm_OP_INVOKE_DIRECT,                        
    ldvm_OP_INVOKE_STATIC,                        
    ldvm_OP_INVOKE_INTERFACE,                     
    ldvm_OP_UNUSED_73,                            
    ldvm_OP_INVOKE_VIRTUAL_RANGE,                 
    ldvm_OP_INVOKE_SUPER_RANGE,                   
    ldvm_OP_INVOKE_DIRECT_RANGE,                  
    ldvm_OP_INVOKE_STATIC_RANGE,                  
    ldvm_OP_INVOKE_INTERFACE_RANGE,               
    ldvm_OP_UNUSED_79,                            
    ldvm_OP_UNUSED_7A,                            
    ldvm_OP_NEG_INT,                              
    ldvm_OP_NOT_INT,                              
    ldvm_OP_NEG_LONG,                             
    ldvm_OP_NOT_LONG,                             
    ldvm_OP_NEG_FLOAT,                            
    ldvm_OP_NEG_DOUBLE,                           
    ldvm_OP_INT_TO_LONG,                          
    ldvm_OP_INT_TO_FLOAT,                         
    ldvm_OP_INT_TO_DOUBLE,                        
    ldvm_OP_LONG_TO_INT,                          
    ldvm_OP_LONG_TO_FLOAT,                        
    ldvm_OP_LONG_TO_DOUBLE,                       
    ldvm_OP_FLOAT_TO_INT,                         
    ldvm_OP_FLOAT_TO_LONG,                        
    ldvm_OP_FLOAT_TO_DOUBLE,                      
    ldvm_OP_DOUBLE_TO_INT,                        
    ldvm_OP_DOUBLE_TO_LONG,                       
    ldvm_OP_DOUBLE_TO_FLOAT,                      
    ldvm_OP_INT_TO_BYTE,                          
    ldvm_OP_INT_TO_CHAR,                          
    ldvm_OP_INT_TO_SHORT,                         
    ldvm_OP_ADD_INT,                              
    ldvm_OP_SUB_INT,                              
    ldvm_OP_MUL_INT,                              
    ldvm_OP_DIV_INT,                              
    ldvm_OP_REM_INT,                              
    ldvm_OP_AND_INT,                              
    ldvm_OP_OR_INT,                               
    ldvm_OP_XOR_INT,                              
    ldvm_OP_SHL_INT,                              
    ldvm_OP_SHR_INT,                              
    ldvm_OP_USHR_INT,                             
    ldvm_OP_ADD_LONG,                             
    ldvm_OP_SUB_LONG,                             
    ldvm_OP_MUL_LONG,                             
    ldvm_OP_DIV_LONG,                             
    ldvm_OP_REM_LONG,                             
    ldvm_OP_AND_LONG,                             
    ldvm_OP_OR_LONG,                              
    ldvm_OP_XOR_LONG,                             
    ldvm_OP_SHL_LONG,                             
    ldvm_OP_SHR_LONG,                             
    ldvm_OP_USHR_LONG,                            
    ldvm_OP_ADD_FLOAT,                            
    ldvm_OP_SUB_FLOAT,                            
    ldvm_OP_MUL_FLOAT,                            
    ldvm_OP_DIV_FLOAT,                            
    ldvm_OP_REM_FLOAT,                            
    ldvm_OP_ADD_DOUBLE,                           
    ldvm_OP_SUB_DOUBLE,                           
    ldvm_OP_MUL_DOUBLE,                           
    ldvm_OP_DIV_DOUBLE,                           
    ldvm_OP_REM_DOUBLE,                           
    ldvm_OP_ADD_INT_2ADDR,                        
    ldvm_OP_SUB_INT_2ADDR,                        
    ldvm_OP_MUL_INT_2ADDR,                        
    ldvm_OP_DIV_INT_2ADDR,                        
    ldvm_OP_REM_INT_2ADDR,                        
    ldvm_OP_AND_INT_2ADDR,                        
    ldvm_OP_OR_INT_2ADDR,                         
    ldvm_OP_XOR_INT_2ADDR,                        
    ldvm_OP_SHL_INT_2ADDR,                        
    ldvm_OP_SHR_INT_2ADDR,                        
    ldvm_OP_USHR_INT_2ADDR,                       
    ldvm_OP_ADD_LONG_2ADDR,                       
    ldvm_OP_SUB_LONG_2ADDR,                       
    ldvm_OP_MUL_LONG_2ADDR,                       
    ldvm_OP_DIV_LONG_2ADDR,                       
    ldvm_OP_REM_LONG_2ADDR,                       
    ldvm_OP_AND_LONG_2ADDR,                       
    ldvm_OP_OR_LONG_2ADDR,                        
    ldvm_OP_XOR_LONG_2ADDR,                       
    ldvm_OP_SHL_LONG_2ADDR,                       
    ldvm_OP_SHR_LONG_2ADDR,                       
    ldvm_OP_USHR_LONG_2ADDR,                      
    ldvm_OP_ADD_FLOAT_2ADDR,                      
    ldvm_OP_SUB_FLOAT_2ADDR,                      
    ldvm_OP_MUL_FLOAT_2ADDR,                      
    ldvm_OP_DIV_FLOAT_2ADDR,                      
    ldvm_OP_REM_FLOAT_2ADDR,                      
    ldvm_OP_ADD_DOUBLE_2ADDR,                     
    ldvm_OP_SUB_DOUBLE_2ADDR,                     
    ldvm_OP_MUL_DOUBLE_2ADDR,                     
    ldvm_OP_DIV_DOUBLE_2ADDR,                     
    ldvm_OP_REM_DOUBLE_2ADDR,                     
    ldvm_OP_ADD_INT_LIT16,                        
    ldvm_OP_RSUB_INT,                             
    ldvm_OP_MUL_INT_LIT16,                        
    ldvm_OP_DIV_INT_LIT16,                        
    ldvm_OP_REM_INT_LIT16,                        
    ldvm_OP_AND_INT_LIT16,                        
    ldvm_OP_OR_INT_LIT16,                         
    ldvm_OP_XOR_INT_LIT16,                        
    ldvm_OP_ADD_INT_LIT8,                         
    ldvm_OP_RSUB_INT_LIT8,                        
    ldvm_OP_MUL_INT_LIT8,                         
    ldvm_OP_DIV_INT_LIT8,                         
    ldvm_OP_REM_INT_LIT8,                         
    ldvm_OP_AND_INT_LIT8,          
    ldvm_OP_OR_INT_LIT8,                          
    ldvm_OP_XOR_INT_LIT8,                                        
    ldvm_OP_SHL_INT_LIT8,                         
    ldvm_OP_SHR_INT_LIT8,                         
    ldvm_OP_USHR_INT_LIT8,                        
    ldvm_OP_IGET_VOLATILE,                        
    ldvm_OP_IPUT_VOLATILE,                        
    ldvm_OP_SGET_VOLATILE,                        
    ldvm_OP_SPUT_VOLATILE,                        
    ldvm_OP_IGET_OBJECT_VOLATILE,                 
    ldvm_OP_IGET_WIDE_VOLATILE,                   
    ldvm_OP_IPUT_WIDE_VOLATILE,                   
    ldvm_OP_SGET_WIDE_VOLATILE,                   
    ldvm_OP_SPUT_WIDE_VOLATILE,                   
    ldvm_OP_BREAKPOINT,                           
    ldvm_OP_THROW_VERIFICATION_ERROR,             
    ldvm_OP_EXECUTE_INLINE,                       
    ldvm_OP_EXECUTE_INLINE_RANGE,                 
    ldvm_OP_INVOKE_OBJECT_INIT_RANGE,             
    ldvm_OP_RETURN_VOID_BARRIER,                  
    ldvm_OP_IGET_QUICK,                           
    ldvm_OP_IGET_WIDE_QUICK,                      
    ldvm_OP_IGET_OBJECT_QUICK,                    
    ldvm_OP_IPUT_QUICK,                           
    ldvm_OP_IPUT_WIDE_QUICK,                      
    ldvm_OP_IPUT_OBJECT_QUICK,                    
    ldvm_OP_INVOKE_VIRTUAL_QUICK,                 
    ldvm_OP_INVOKE_VIRTUAL_QUICK_RANGE,           
    ldvm_OP_INVOKE_SUPER_QUICK,                   
    ldvm_OP_INVOKE_SUPER_QUICK_RANGE,             
    ldvm_OP_IPUT_OBJECT_VOLATILE,                 
    ldvm_OP_SGET_OBJECT_VOLATILE,                 
    ldvm_OP_SPUT_OBJECT_VOLATILE,                 
    ldvm_OP_DISPATCH_FF,                          
    ldvm_OP_CONST_CLASS_JUMBO,                    
    ldvm_OP_CHECK_CAST_JUMBO,                     
    ldvm_OP_INSTANCE_OF_JUMBO,                    
    ldvm_OP_NEW_INSTANCE_JUMBO,                   
    ldvm_OP_NEW_ARRAY_JUMBO,                      
    ldvm_OP_FILLED_NEW_ARRAY_JUMBO,               
    ldvm_OP_IGET_JUMBO,                           
    ldvm_OP_IGET_WIDE_JUMBO,                      
    ldvm_OP_IGET_OBJECT_JUMBO,                    
    ldvm_OP_IGET_BOOLEAN_JUMBO,                   
    ldvm_OP_IGET_BYTE_JUMBO,                      
    ldvm_OP_IGET_CHAR_JUMBO,                      
    ldvm_OP_IGET_SHORT_JUMBO,                     
    ldvm_OP_IPUT_JUMBO,                           
    ldvm_OP_IPUT_WIDE_JUMBO,                      
    ldvm_OP_IPUT_OBJECT_JUMBO,                    
    ldvm_OP_IPUT_BOOLEAN_JUMBO,                   
    ldvm_OP_IPUT_BYTE_JUMBO,                      
    ldvm_OP_IPUT_CHAR_JUMBO,                      
    ldvm_OP_IPUT_SHORT_JUMBO,                     
    ldvm_OP_SGET_JUMBO,                           
    ldvm_OP_SGET_WIDE_JUMBO,                      
    ldvm_OP_SGET_OBJECT_JUMBO,                    
    ldvm_OP_SGET_BOOLEAN_JUMBO,                   
    ldvm_OP_SGET_BYTE_JUMBO,                      
    ldvm_OP_SGET_CHAR_JUMBO,                      
    ldvm_OP_SGET_SHORT_JUMBO,                     
    ldvm_OP_SPUT_JUMBO,                           
    ldvm_OP_SPUT_WIDE_JUMBO,                      
    ldvm_OP_SPUT_OBJECT_JUMBO,                    
    ldvm_OP_SPUT_BOOLEAN_JUMBO,                   
    ldvm_OP_SPUT_BYTE_JUMBO,                      
    ldvm_OP_SPUT_CHAR_JUMBO,                      
    ldvm_OP_SPUT_SHORT_JUMBO,                     
    ldvm_OP_INVOKE_VIRTUAL_JUMBO,                 
    ldvm_OP_INVOKE_SUPER_JUMBO,                   
    ldvm_OP_INVOKE_DIRECT_JUMBO,                  
    ldvm_OP_INVOKE_STATIC_JUMBO,                  
    ldvm_OP_INVOKE_INTERFACE_JUMBO,               
    ldvm_OP_UNUSED_27FF,                          
    ldvm_OP_UNUSED_28FF,                          
    ldvm_OP_UNUSED_29FF,                          
    ldvm_OP_UNUSED_2AFF,                          
    ldvm_OP_UNUSED_2BFF,                          
    ldvm_OP_UNUSED_2CFF,                          
    ldvm_OP_UNUSED_2DFF,                          
    ldvm_OP_UNUSED_2EFF,                          
    ldvm_OP_UNUSED_2FFF,                          
    ldvm_OP_UNUSED_30FF,                          
    ldvm_OP_UNUSED_31FF,                          
    ldvm_OP_UNUSED_32FF,                          
    ldvm_OP_UNUSED_33FF,                          
    ldvm_OP_UNUSED_34FF,                          
    ldvm_OP_UNUSED_35FF,                          
    ldvm_OP_UNUSED_36FF,                          
    ldvm_OP_UNUSED_37FF,                          
    ldvm_OP_UNUSED_38FF,                          
    ldvm_OP_UNUSED_39FF,                          
    ldvm_OP_UNUSED_3AFF,                          
    ldvm_OP_UNUSED_3BFF,                          
    ldvm_OP_UNUSED_3CFF,                          
    ldvm_OP_UNUSED_3DFF,                          
    ldvm_OP_UNUSED_3EFF,                          
    ldvm_OP_UNUSED_3FFF,                          
    ldvm_OP_UNUSED_40FF,                          
    ldvm_OP_UNUSED_41FF,                          
    ldvm_OP_UNUSED_42FF,                          
    ldvm_OP_UNUSED_43FF,                          
    ldvm_OP_UNUSED_44FF,                          
    ldvm_OP_UNUSED_45FF,                          
    ldvm_OP_UNUSED_46FF,                          
    ldvm_OP_UNUSED_47FF,                          
    ldvm_OP_UNUSED_48FF,                          
    ldvm_OP_UNUSED_49FF,                          
    ldvm_OP_UNUSED_4AFF,                          
    ldvm_OP_UNUSED_4BFF,                          
    ldvm_OP_UNUSED_4CFF,                          
    ldvm_OP_UNUSED_4DFF,                          
    ldvm_OP_UNUSED_4EFF,                          
    ldvm_OP_UNUSED_4FFF,                          
    ldvm_OP_UNUSED_50FF,                          
    ldvm_OP_UNUSED_51FF,                          
    ldvm_OP_UNUSED_52FF,                          
    ldvm_OP_UNUSED_53FF,                          
    ldvm_OP_UNUSED_54FF,                          
    ldvm_OP_UNUSED_55FF,                          
    ldvm_OP_UNUSED_56FF,                          
    ldvm_OP_UNUSED_57FF,                          
    ldvm_OP_UNUSED_58FF,                          
    ldvm_OP_UNUSED_59FF,                          
    ldvm_OP_UNUSED_5AFF,                          
    ldvm_OP_UNUSED_5BFF,                          
    ldvm_OP_UNUSED_5CFF,                          
    ldvm_OP_UNUSED_5DFF,                          
    ldvm_OP_UNUSED_5EFF,                          
    ldvm_OP_UNUSED_5FFF,                          
    ldvm_OP_UNUSED_60FF,                          
    ldvm_OP_UNUSED_61FF,                          
    ldvm_OP_UNUSED_62FF,                          
    ldvm_OP_UNUSED_63FF,                          
    ldvm_OP_UNUSED_64FF,                          
    ldvm_OP_UNUSED_65FF,                          
    ldvm_OP_UNUSED_66FF,                          
    ldvm_OP_UNUSED_67FF,                          
    ldvm_OP_UNUSED_68FF,                          
    ldvm_OP_UNUSED_69FF,                          
    ldvm_OP_UNUSED_6AFF,                          
    ldvm_OP_UNUSED_6BFF,                          
    ldvm_OP_UNUSED_6CFF,                          
    ldvm_OP_UNUSED_6DFF,                          
    ldvm_OP_UNUSED_6EFF,                          
    ldvm_OP_UNUSED_6FFF,                          
    ldvm_OP_UNUSED_70FF,                          
    ldvm_OP_UNUSED_71FF,                          
    ldvm_OP_UNUSED_72FF,                          
    ldvm_OP_UNUSED_73FF,                          
    ldvm_OP_UNUSED_74FF,                          
    ldvm_OP_UNUSED_75FF,                          
    ldvm_OP_UNUSED_76FF,                          
    ldvm_OP_UNUSED_77FF,                          
    ldvm_OP_UNUSED_78FF,                          
    ldvm_OP_UNUSED_79FF,                          
    ldvm_OP_UNUSED_7AFF,                          
    ldvm_OP_UNUSED_7BFF,                          
    ldvm_OP_UNUSED_7CFF,                          
    ldvm_OP_UNUSED_7DFF,                          
    ldvm_OP_UNUSED_7EFF,                          
    ldvm_OP_UNUSED_7FFF,                          
    ldvm_OP_UNUSED_80FF,                          
    ldvm_OP_UNUSED_81FF,                          
    ldvm_OP_UNUSED_82FF,                          
    ldvm_OP_UNUSED_83FF,                          
    ldvm_OP_UNUSED_84FF,                          
    ldvm_OP_UNUSED_85FF,                          
    ldvm_OP_UNUSED_86FF,                          
    ldvm_OP_UNUSED_87FF,                          
    ldvm_OP_UNUSED_88FF,                          
    ldvm_OP_UNUSED_89FF,                          
    ldvm_OP_UNUSED_8AFF,                          
    ldvm_OP_UNUSED_8BFF,                          
    ldvm_OP_UNUSED_8CFF,                          
    ldvm_OP_UNUSED_8DFF,                          
    ldvm_OP_UNUSED_8EFF,                          
    ldvm_OP_UNUSED_8FFF,                          
    ldvm_OP_UNUSED_90FF,                          
    ldvm_OP_UNUSED_91FF,                          
    ldvm_OP_UNUSED_92FF,                          
    ldvm_OP_UNUSED_93FF,                          
    ldvm_OP_UNUSED_94FF,                          
    ldvm_OP_UNUSED_95FF,                          
    ldvm_OP_UNUSED_96FF,                          
    ldvm_OP_UNUSED_97FF,                          
    ldvm_OP_UNUSED_98FF,                          
    ldvm_OP_UNUSED_99FF,                          
    ldvm_OP_UNUSED_9AFF,                          
    ldvm_OP_UNUSED_9BFF,                          
    ldvm_OP_UNUSED_9CFF,                          
    ldvm_OP_UNUSED_9DFF,                          
    ldvm_OP_UNUSED_9EFF,                          
    ldvm_OP_UNUSED_9FFF,                          
    ldvm_OP_UNUSED_A0FF,                          
    ldvm_OP_UNUSED_A1FF,                          
    ldvm_OP_UNUSED_A2FF,                          
    ldvm_OP_UNUSED_A3FF,                          
    ldvm_OP_UNUSED_A4FF,                          
    ldvm_OP_UNUSED_A5FF,                          
    ldvm_OP_UNUSED_A6FF,                          
    ldvm_OP_UNUSED_A7FF,                          
    ldvm_OP_UNUSED_A8FF,                          
    ldvm_OP_UNUSED_A9FF,                          
    ldvm_OP_UNUSED_AAFF,                          
    ldvm_OP_UNUSED_ABFF,                          
    ldvm_OP_UNUSED_ACFF,                          
    ldvm_OP_UNUSED_ADFF,                          
    ldvm_OP_UNUSED_AEFF,                          
    ldvm_OP_UNUSED_AFFF,                          
    ldvm_OP_UNUSED_B0FF,                          
    ldvm_OP_UNUSED_B1FF,                          
    ldvm_OP_UNUSED_B2FF,                          
    ldvm_OP_UNUSED_B3FF,                          
    ldvm_OP_UNUSED_B4FF,                          
    ldvm_OP_UNUSED_B5FF,                          
    ldvm_OP_UNUSED_B6FF,                          
    ldvm_OP_UNUSED_B7FF,                          
    ldvm_OP_UNUSED_B8FF,                          
    ldvm_OP_UNUSED_B9FF,                          
    ldvm_OP_UNUSED_BAFF,                          
    ldvm_OP_UNUSED_BBFF,                          
    ldvm_OP_UNUSED_BCFF,                          
    ldvm_OP_UNUSED_BDFF,                          
    ldvm_OP_UNUSED_BEFF,                          
    ldvm_OP_UNUSED_BFFF,                          
    ldvm_OP_UNUSED_C0FF,                          
    ldvm_OP_UNUSED_C1FF,                          
    ldvm_OP_UNUSED_C2FF,                          
    ldvm_OP_UNUSED_C3FF,                          
    ldvm_OP_UNUSED_C4FF,                          
    ldvm_OP_UNUSED_C5FF,                          
    ldvm_OP_UNUSED_C6FF,                          
    ldvm_OP_UNUSED_C7FF,                          
    ldvm_OP_UNUSED_C8FF,                          
    ldvm_OP_UNUSED_C9FF,                          
    ldvm_OP_UNUSED_CAFF,                          
    ldvm_OP_UNUSED_CBFF,                          
    ldvm_OP_UNUSED_CCFF,                          
    ldvm_OP_UNUSED_CDFF,                          
    ldvm_OP_UNUSED_CEFF,                          
    ldvm_OP_UNUSED_CFFF,                          
    ldvm_OP_UNUSED_D0FF,                          
    ldvm_OP_UNUSED_D1FF,                          
    ldvm_OP_UNUSED_D2FF,                          
    ldvm_OP_UNUSED_D3FF,                          
    ldvm_OP_UNUSED_D4FF,                          
    ldvm_OP_UNUSED_D5FF,                          
    ldvm_OP_UNUSED_D6FF,                          
    ldvm_OP_UNUSED_D7FF,                          
    ldvm_OP_UNUSED_D8FF,                          
    ldvm_OP_UNUSED_D9FF,                          
    ldvm_OP_UNUSED_DAFF,                          
    ldvm_OP_UNUSED_DBFF,                          
    ldvm_OP_UNUSED_DCFF,                          
    ldvm_OP_UNUSED_DDFF,                          
    ldvm_OP_UNUSED_DEFF,                          
    ldvm_OP_UNUSED_DFFF,                          
    ldvm_OP_UNUSED_E0FF,                          
    ldvm_OP_UNUSED_E1FF,                          
    ldvm_OP_UNUSED_E2FF,                          
    ldvm_OP_UNUSED_E3FF,                          
    ldvm_OP_UNUSED_E4FF,                          
    ldvm_OP_UNUSED_E5FF,                          
    ldvm_OP_UNUSED_E6FF,                          
    ldvm_OP_UNUSED_E7FF,                          
    ldvm_OP_UNUSED_E8FF,                          
    ldvm_OP_UNUSED_E9FF,                          
    ldvm_OP_UNUSED_EAFF,                          
    ldvm_OP_UNUSED_EBFF,                          
    ldvm_OP_UNUSED_ECFF,                          
    ldvm_OP_UNUSED_EDFF,                          
    ldvm_OP_UNUSED_EEFF,                          
    ldvm_OP_UNUSED_EFFF,                          
    ldvm_OP_UNUSED_F0FF,                          
    ldvm_OP_UNUSED_F1FF,                          
    ldvm_OP_INVOKE_OBJECT_INIT_JUMBO,             
    ldvm_OP_IGET_VOLATILE_JUMBO,                  
    ldvm_OP_IGET_WIDE_VOLATILE_JUMBO,             
    ldvm_OP_IGET_OBJECT_VOLATILE_JUMBO,           
    ldvm_OP_IPUT_VOLATILE_JUMBO,                  
    ldvm_OP_IPUT_WIDE_VOLATILE_JUMBO,             
    ldvm_OP_IPUT_OBJECT_VOLATILE_JUMBO,           
    ldvm_OP_SGET_VOLATILE_JUMBO,                  
    ldvm_OP_SGET_WIDE_VOLATILE_JUMBO,             
    ldvm_OP_SGET_OBJECT_VOLATILE_JUMBO,           
    ldvm_OP_SPUT_VOLATILE_JUMBO,                  
    ldvm_OP_SPUT_WIDE_VOLATILE_JUMBO,             
    ldvm_OP_SPUT_OBJECT_VOLATILE_JUMBO,           
    ldvm_OP_THROW_VERIFICATION_ERROR_JUMBO,       
    /* END(libdex-goto-table) */                
};

#endif