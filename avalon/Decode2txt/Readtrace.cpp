#include "trace.h"
#include "method.h"

typedef void (*Handler)( Loccs_opcode lo, FILE* fpw );

void output_opcode( const Loccs_opcode& lo, FILE* fpw )
{
	fprintf( fpw, "\n---------------------------------------------------------------\n");
	//fprintf( fpw, "Thread ID : %x\n",lo.threadId);
	//fprintf( fpw, "string : %s\n",lo.extraData);

	fprintf( fpw, "pc = %08x\n", lo.pc);
	fprintf( fpw, "class : %s\n", lo.methodDescriptor);
	fprintf( fpw, "method : %s\n", lo.methodName);
	//fprintf( fpw, "logcontent:%s\n", lo.logcontent);

	fprintf( fpw, "Reg\n" );
	for( size_t i = 0; i < RegMaxNum; i++ )
	{
		fprintf( fpw, "%08x\t", lo.reg[i] );
	}

	fprintf( fpw, "\n" );


	// using handler function to deal with opcode's details
	uint16_t inst = lo.inst[0];
	Handler deal_opcode = (Handler) opcodeHandlers[inst & 0xff];
	(*deal_opcode)( lo, fpw );
}

int main( int argc, char* argv[] )
{
	if ( argc != 3 )
	{
		puts("Usage: parser record_file result_file");
		return -1;
	}


	FILE* fp = fopen( argv[1], "rb" ); // DIAOS opcode record binary file
	FILE* fpw = fopen( argv[2], "wb"); // after parsing

	if ( fp == NULL )
	{
		printf( "the record file does not exist\n");
		return -1;
	}

	if ( fpw == NULL )
	{
		printf( "fail to open result file\n");
		return -1;
	}

	uint32_t inst_num = 0;
	Loccs_opcode lo;

	while ( !feof(fp) )
	{
		if ( fread( &lo, sizeof(Loccs_opcode), 1, fp ) == 1 )
		{
			output_opcode( lo, fpw );
			++inst_num;
		}
	}

	puts( "parse instruction finished\n" );
	printf( "instruction number %d\n", inst_num );

	fclose( fp );
	fclose( fpw );

	system("PAUSE");
	return 0;
}

