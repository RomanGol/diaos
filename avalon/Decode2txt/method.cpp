#include "method.h"

#define PRINT_REG void()
#pragma warning(disable:4101)

u4 getReg(const Loccs_opcode lo, u2 offset) {
	u4 ret = 0;
	if ( offset > 16 ) {
		ret = 0xFFFFFFFF;
		printf("reg overflow\n");
		printf("pc = %08x\n", lo.pc);
		printf("class : %s\n",lo.methodDescriptor);
		printf("method : %s\n",lo.methodName);
	}
	else
		ret = lo.reg[offset];
	
	return ret;
}

void ldvm_OP_NOP(const Loccs_opcode lo, FILE* fpw)
{
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw, "NOP");
	PRINT_REG;
}

void ldvm_OP_MOVE(const Loccs_opcode lo, FILE* fpw){

	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);

	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move v%d,v%d (v%d=0x%08x)", vdst, vsrc1,vdst, getReg(lo, vsrc1));
	PRINT_REG;
}
void ldvm_OP_MOVE_FROM16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (inst >> 8);
	vsrc1 = (lo.inst[1]);
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move/from16 v%d,v%d (v%d=0x%08x)", vdst, vsrc1,vdst, getReg(lo, vsrc1));
}
void ldvm_OP_MOVE_16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (lo.inst[1]);
	vsrc1 = (lo.inst[(2)]);
	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move/16 v%d,v%d (v%d=0x%08x)", vdst, vsrc1,vdst, getReg(lo, vsrc1));
}

void ldvm_OP_ADD_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|add-int v%d,v%d", vdst, vsrc1); 
}
void ldvm_OP_CONST_4(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	s4 tmp;
	vdst = ((inst >> 8) & 0x0f);
	tmp = (s4) (((inst) >> 12) << 28) >> 28;

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|const/4 v%d,#0x%02x", vdst, (s4)tmp);
}
void ldvm_OP_CONST_16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (inst >> 8);
	vsrc1 = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|const/16 v%d,#0x%04x", vdst, (s2)vsrc1);
}
void ldvm_OP_CONST(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	u4 tmp;
	vdst = (inst >> 8);
	tmp = (lo.inst[1]);
	
	tmp |= (u4)(lo.inst[(2)]) << 16;
	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|const v%d,#0x%08x", vdst, tmp);
}
void ldvm_OP_CONST_HIGH16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (inst >> 8);
	vsrc1 = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|const/high16 v%d,#0x%04x0000", vdst, vsrc1);
}
void ldvm_OP_MOVE_WIDE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 inst = (lo.inst[0]); 
	u2 vdst = ((inst >> 8) & 0x0f);
	u2 vsrc1 = ((inst) >> 12);
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move-wide v%d,v%d (v%d=0x%08llx)", vdst, vsrc1,vdst, getReg(lo, vsrc1));
}
void ldvm_OP_MOVE_WIDE_FROM16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (inst >> 8);
	vsrc1 = (lo.inst[1]);
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move-wide/from16 v%d,v%d  (v%d=0x%08llx)", vdst, vsrc1, vdst, getReg(lo, vsrc1));
}
void ldvm_OP_MOVE_WIDE_16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (lo.inst[1]);
	vsrc1 = (lo.inst[(2)]);
	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move-wide/16 v%d,v%d  (v%d=0x%08llx)", vdst, vsrc1, vdst, getReg(lo, vsrc1));
}
void ldvm_OP_MOVE_OBJECT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);	
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move-object v%d,v%d (v%d=0x%08x)", vdst, vsrc1, vdst, getReg(lo, vsrc1));
	

}
void ldvm_OP_MOVE_OBJECT_FROM16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (inst >> 8);
	vsrc1 = (lo.inst[1]);
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move-object/from16 v%d,v%d (v%d=0x%08x)", vdst, vsrc1, vdst, getReg(lo, vsrc1));
}
void ldvm_OP_MOVE_OBJECT_16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (lo.inst[1]);
	vsrc1 = (lo.inst[(2)]);
	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move-object/16 v%d,v%d (v%d=0x%08x)", vdst, vsrc1, vdst, getReg(lo, vsrc1));

}
void ldvm_OP_MOVE_RESULT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (inst >> 8);
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move-result v%d (v%d = ) ----still uncompleted", vdst, vdst);
}
void ldvm_OP_MOVE_RESULT_WIDE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst) >> 8);
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move-result-wide v%d (v%d = )----still uncompleted ", vdst, vdst);
}
void ldvm_OP_MOVE_RESULT_OBJECT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (inst >> 8);
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move-result-object v%d (v%d = )----still uncompleted ", vdst, vdst);
}
void ldvm_OP_MOVE_EXCEPTION(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (inst >> 8);
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|move-exception v%d", vdst);
}
void ldvm_OP_RETURN_VOID(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|return-void");
}
void ldvm_OP_RETURN(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = (inst >> 8);
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|return v%d", vsrc1);
}
void ldvm_OP_RETURN_WIDE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = (inst >> 8);
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|return-wide v%d", vsrc1);
}
void ldvm_OP_RETURN_OBJECT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = (inst >> 8);
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|return-object v%d", vsrc1);
}
void ldvm_OP_CONST_WIDE_16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (inst >> 8);
	vsrc1 = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|const/16 v%d,#0x%04x", vdst, (s2)vsrc1);
}
void ldvm_OP_CONST_WIDE_32(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	u4 tmp;
	vdst = (inst >> 8);
	tmp = (lo.inst[1]);
	tmp |= (u4)(lo.inst[(2)]) << 16;

	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
    fprintf(fpw,"Inst_Parse:|const-wide/32 v%d,#0x%08x", vdst, tmp);
}
void ldvm_OP_CONST_WIDE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	u8 tmp;
	vdst = (inst >> 8);
	tmp = (lo.inst[1]);
	tmp |= (u8)(lo.inst[(2)]) << 16;
	tmp |= (u8)(lo.inst[(3)]) << 32;
	tmp |= (u8)(lo.inst[(4)]) << 48;

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|const-wide v%d,#0x%08llx", vdst, tmp);
}
void ldvm_OP_CONST_WIDE_HIGH16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (inst >> 8);
	vsrc1 = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|const-wide/high16 v%d,#0x%04x000000000000", vdst, vsrc1);
}
void ldvm_OP_CONST_STRING(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	//StringObject* strObj;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|const-string v%d string@0x%04x", vdst, ref);
}
void ldvm_OP_CONST_STRING_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	//StringObject* strObj;
	u4 tmp;
	vdst = (inst >> 8);
	tmp = (lo.inst[1]);
	tmp |= (u4)(lo.inst[(2)]) << 16;

	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
    fprintf(fpw,"Inst_Parse:|const-string/jumbo v%d string@0x%08x", vdst, tmp);
}
void ldvm_OP_CONST_CLASS(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	//ClassObject* clazz;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
    fprintf(fpw,"Inst_Parse:|const-class v%d class@0x%04x", vdst, ref);
}
void ldvm_OP_MONITOR_ENTER(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	//Object* obj;
	vsrc1 = (inst >> 8);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|monitor-enter v%d (0x%08x)", vsrc1, getReg(lo, vsrc1));
}
//这个opcode的具体作用还不明
void ldvm_OP_MONITOR_EXIT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vsrc1 = (inst >> 8);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|monitor-exit v%d (0x%08x)", vsrc1, getReg(lo, vsrc1));
}
void ldvm_OP_CHECK_CAST(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	//ClassObject* clazz;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vsrc1 = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|check-cast v%d,class@0x%04x", vsrc1, ref);
}
void ldvm_OP_INSTANCE_OF(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	//ClassObject* clazz;
	//Object* obj;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|instance-of v%d,v%d,class@0x%04x", vdst, vsrc1, ref);
}
void ldvm_OP_ARRAY_LENGTH(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	//ArrayObject* arrayObj;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	//fprintf(fpw,"Inst_Parse:|array-length v%d,v%d  (%p)", vdst, vsrc1, arrayObj);
	fprintf(fpw,"Inst_Parse:|array-length v%d,v%d ------still uncompleted", vdst, vsrc1);
}
void ldvm_OP_NEW_INSTANCE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	//ClassObject* clazz;
	//Object* newObj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|new-instance v%d,class@0x%04x", vdst, ref);
}
void ldvm_OP_NEW_ARRAY(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	/*ClassObject* arrayClass;
	ArrayObject* newArray;
	s4 length;
	(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);*/
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|new-array v%d,v%d,class@0x%04x  (%d elements)", vdst, vsrc1, ref, (s4) getReg(lo, vsrc1));
}
void ldvm_OP_FILLED_NEW_ARRAY(const Loccs_opcode lo, FILE* fpw){
	u4 ref, arg5;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	arg5 = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = lo.inst[1];
	vdst = lo.inst[2];

	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|filled-new-array args=%d @0x%04x {regs=0x%04x %x}",
		vsrc1, ref, vdst, arg5);
}
void ldvm_OP_FILLED_NEW_ARRAY_RANGE(const Loccs_opcode lo, FILE* fpw){
	u4 ref, arg5;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	arg5 = -1;
	vsrc1 = ((inst) >> 8);
	ref = lo.inst[1];
	vdst = lo.inst[2];

	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|filled-new-array-range args=%d @0x%04x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst + vsrc1 - 1);
}
void ldvm_OP_FILL_ARRAY_DATA(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	s4 offset;
	/*const u2* arrayData;
	ArrayObject* arrayObj;
	(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);*/
	vsrc1 = (inst >> 8);
	offset = (lo.inst[1]) | (((s4) (lo.inst[(2)])) << 16);

	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|fill-array-data v%d +0x%04x", vsrc1, offset);
}
void ldvm_OP_THROW(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vsrc1 = (inst >> 8);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|throw v%d  (%p)", vsrc1, (void*)(getReg(lo, vsrc1)));
}
void ldvm_OP_GOTO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vdst = (inst >> 8);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	if ((s1)vdst < 0)
		fprintf(fpw,"Inst_Parse:|goto -0x%02x", -((s1)vdst));
	else
		fprintf(fpw,"Inst_Parse:|goto +0x%02x", ((s1)vdst));
	fprintf(fpw,"> branch taken");

}
void ldvm_OP_GOTO_16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	s4 offset = (s2) (lo.inst[(1)]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	if (offset < 0)
		fprintf(fpw,"Inst_Parse:|goto/16 -0x%04x", -offset);
	else
		fprintf(fpw,"Inst_Parse:|goto/16 +0x%04x", offset);
	fprintf(fpw,"> branch taken");
}
void ldvm_OP_GOTO_32(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	s4 offset = (lo.inst[1]);
	offset |= ((s4) (lo.inst[(2)])) << 16;

	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	if (offset < 0)
		fprintf(fpw,"Inst_Parse:|goto/32 -0x%08x", -offset);
	else
		fprintf(fpw,"Inst_Parse:|goto/32 +0x%08x", offset);
	fprintf(fpw,"> branch taken");
}
void ldvm_OP_PACKED_SWITCH(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	const u2* switchData;
	u4 testVal;
	s4 offset;
	vsrc1 = (inst >> 8);
	offset = (lo.inst[1]) | (((s4) (lo.inst[(2)])) << 16);

	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|packed-switch v%d +0x%04x------still uncompleted", vsrc1, offset);
	//fprintf(fpw,"> branch taken (0x%04x)", offset);
	//fprintf(fpw,"> branch taken");

}
void ldvm_OP_SPARSE_SWITCH(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	const u2* switchData;
	u4 testVal;
	s4 offset;
	vsrc1 = (inst >> 8);
	offset = (lo.inst[1]) | (((s4) (lo.inst[(2)])) << 16);

	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sparse-switch v%d +0x%04x------still uncompleted", vsrc1, offset);
	//fprintf(fpw,"> branch taken (0x%04x)", offset);
	//fprintf(fpw,"> branch taken");

}
void ldvm_OP_CMPL_FLOAT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	int result;
	u2 regs;
	float val1, val2;
	vdst = (inst >> 8);
	regs = (lo.inst[1]);
	vsrc1 = regs & 0xff;
	vsrc2 = regs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|cmp%s v%d,v%d,v%d", ("l-float"), vdst, vsrc1, vsrc2);

}
void ldvm_OP_CMPG_FLOAT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	int result;
	u2 regs;
	float val1, val2;
	vdst = (inst >> 8);
	regs = (lo.inst[1]);
	vsrc1 = regs & 0xff;
	vsrc2 = (regs >> 8);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|cmp%s v%d,v%d,v%d", ("g-float"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_CMPL_DOUBLE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	int result;
	u2 regs;
	double val1, val2;
	vdst = (inst >> 8);
	regs = (lo.inst[1]); 
	vsrc1 = regs & 0xff; 
	vsrc2 = regs >> 8; 
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|cmp%s v%d,v%d,v%d", ("l-double"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_CMPG_DOUBLE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	int result;
	u2 regs;
	double val1, val2;
	vdst = (inst >> 8);
	regs = (lo.inst[1]);
	vsrc1 = regs & 0xff;
	vsrc2 = regs >> 8; 
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|cmp%s v%d,v%d,v%d", ("g-double"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_CMP_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	int result; u2 regs;
	s8 val1, val2;
	vdst = (inst >> 8);
	regs = (lo.inst[1]); 
	vsrc1 = regs & 0xff; 
	vsrc2 = regs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|cmp%s v%d,v%d,v%d", ("-long"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_IF_EQ(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = ((inst >> 8) & 0x0f);
	vsrc2 = ((inst) >> 12); 
	//if ((s4) (getReg(lo, vsrc1)) == (s4) (lo.reg[(vsrc2)])) 
	int branchOffset = (s2)(lo.inst[1]);


	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|if-%s v%d,v%d,+0x%04x", ("eq"), vsrc1, vsrc2, branchOffset);
}
void ldvm_OP_IF_NE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = ((inst >> 8) & 0x0f);
	vsrc2 = ((inst) >> 12);
	//if ((s4) (getReg(lo, vsrc1)) != (s4) (lo.reg[(vsrc2)]))
	int branchOffset = (s2)(lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|if-%s v%d,v%d,+0x%04x", ("ne"), vsrc1, vsrc2, branchOffset);
}
void ldvm_OP_IF_GE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = ((inst >> 8) & 0x0f);
	vsrc2 = ((inst) >> 12);
	//if ((s4) (getReg(lo, vsrc1)) >= (s4) (lo.reg[(vsrc2)]))
	int branchOffset = (s2)(lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|if-%s v%d,v%d,+0x%04x", ("ge"), vsrc1, vsrc2, branchOffset);
}
void ldvm_OP_IF_LT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = ((inst >> 8) & 0x0f);
	vsrc2 = ((inst) >> 12);
	//if ((s4) (getReg(lo, vsrc1)) < (s4) (lo.reg[(vsrc2)]))
	int branchOffset = (s2)(lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|if-%s v%d,v%d,+0x%04x", ("lt"), vsrc1, vsrc2, branchOffset);
}
void ldvm_OP_IF_GT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = ((inst >> 8) & 0x0f);
	vsrc2 = ((inst) >> 12);
	//if ((s4) (getReg(lo, vsrc1)) > (s4) (lo.reg[(vsrc2)])) 
	int branchOffset = (s2)(lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|if-%s v%d,v%d,+0x%04x", ("gt"), vsrc1, vsrc2, branchOffset);
}
void ldvm_OP_IF_LE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = ((inst >> 8) & 0x0f);
	vsrc2 = ((inst) >> 12);
	//if ((s4) (getReg(lo, vsrc1)) <= (s4) (lo.reg[(vsrc2)]))
	int branchOffset = (s2)(lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|if-%s v%d,v%d,+0x%04x", ("le"), vsrc1, vsrc2, branchOffset);
}

void ldvm_OP_IF_EQZ(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = (inst >> 8);
	//if ((s4) (getReg(lo, vsrc1)) == 0) 
	int branchOffset = (s2)(lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|if-%s v%d,+0x%04x", ("eqz"), vsrc1, branchOffset);
}
void ldvm_OP_IF_NEZ(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = (inst >> 8);
	//if ((s4) (getReg(lo, vsrc1)) != 0) 
	int branchOffset = (s2)(lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|if-%s v%d,+0x%04x", ("nez"), vsrc1, branchOffset);
}
void ldvm_OP_IF_LTZ(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = (inst >> 8);
	//if ((s4) (getReg(lo, vsrc1)) < 0) 
	int branchOffset = (s2)(lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|if-%s v%d,+0x%04x", ("ltz"), vsrc1, branchOffset);
}
void ldvm_OP_IF_GEZ(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = (inst >> 8);
	//if ((s4) (getReg(lo, vsrc1)) >= 0) 
	int branchOffset = (s2)(lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|if-%s v%d,+0x%04x", ("gez"), vsrc1, branchOffset);
}
void ldvm_OP_IF_GTZ(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = (inst >> 8);
	//if ((s4) (getReg(lo, vsrc1)) > 0) 
		int branchOffset = (s2)(lo.inst[1]);
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|if-%s v%d,+0x%04x", ("gtz"), vsrc1, branchOffset);
}
void ldvm_OP_IF_LEZ(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = (inst >> 8);
	//if ((s4) (getReg(lo, vsrc1)) <= 0) 
		int branchOffset = (s2)(lo.inst[1]);
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|if-%s v%d,+0x%04x", ("lez"), vsrc1, branchOffset);
}
void ldvm_OP_UNUSED_3E(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_3E is void.\n");
}
void ldvm_OP_UNUSED_3F(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_3F is void.\n");
}
void ldvm_OP_UNUSED_40(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_40 is void.\n");
}
void ldvm_OP_UNUSED_41(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_41 is void.\n");
}
void ldvm_OP_UNUSED_42(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_42 is void.\n");
}
void ldvm_OP_UNUSED_43(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_43 is void.\n");
}
void ldvm_OP_AGET(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);

	//ArrayObject* arrayObj;
	u2 arrayInfo;

	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	//arrayObj = (ArrayObject*) (getReg(lo, vsrc1));

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aget%s v%d,v%d,v%d", (""), vdst, vsrc1, vsrc2);
}
void ldvm_OP_AGET_WIDE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aget%s v%d,v%d,v%d", ("-wide"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_AGET_OBJECT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aget%s v%d,v%d,v%d", ("-object"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_AGET_BOOLEAN(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aget%s v%d,v%d,v%d", ("-boolean"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_AGET_BYTE(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aget%s v%d,v%d,v%d", ("-byte"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_AGET_CHAR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aget%s v%d,v%d,v%d", ("-char"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_AGET_SHORT(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aget%s v%d,v%d,v%d", ("-short"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_APUT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aput%s v%d,v%d,v%d", (""), vdst, vsrc1, vsrc2);
}
void ldvm_OP_APUT_WIDE(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aput%s v%d,v%d,v%d", ("-wide"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_APUT_OBJECT(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	//Object* obj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	//arrayObj = (ArrayObject*) (getReg(lo, vsrc1));
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aput%s v%d,v%d,v%d", "-object", vdst, vsrc1, vsrc2);
}
void ldvm_OP_APUT_BOOLEAN(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	//arrayObj = (ArrayObject*) (getReg(lo, vsrc1));
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aput%s v%d,v%d,v%d", ("-boolean"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_APUT_BYTE(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aput%s v%d,v%d,v%d", ("-byte"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_APUT_CHAR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	//arrayObj = (ArrayObject*) (getReg(lo, vsrc1));
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aput%s v%d,v%d,v%d", ("-char"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_APUT_SHORT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ArrayObject* arrayObj;
	u2 arrayInfo;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = (inst >> 8);
	arrayInfo = (lo.inst[1]);
	vsrc1 = arrayInfo & 0xff;
	vsrc2 = arrayInfo >> 8;
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|aput%s v%d,v%d,v%d", ("-short"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_IGET(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s v%d,v%d,field@0x%04x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_WIDE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s v%d,v%d,field@0x%04x", ("-wide"), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_OBJECT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s v%d,v%d,field@0x%04x", ("-object"), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_BOOLEAN(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s v%d,v%d,field@0x%04x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_BYTE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s v%d,v%d,field@0x%04x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_CHAR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s v%d,v%d,field@0x%04x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_SHORT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s v%d,v%d,field@0x%04x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s v%d,v%d,field@0x%04x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_WIDE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s v%d,v%d,field@0x%04x", ("-wide"), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_OBJECT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s v%d,v%d,field@0x%04x", ("-object"), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_BOOLEAN(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s v%d,v%d,field@0x%04x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_BYTE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s v%d,v%d,field@0x%04x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_CHAR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s v%d,v%d,field@0x%04x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_SHORT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s v%d,v%d,field@0x%04x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_SGET(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s v%d,sfield@0x%04x", (""), vdst, ref);
}
void ldvm_OP_SGET_WIDE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s v%d,sfield@0x%04x", (""), vdst, ref);
}
void ldvm_OP_SGET_OBJECT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s v%d,sfield@0x%04x", ("-object"), vdst, ref);
}
void ldvm_OP_SGET_BOOLEAN(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s v%d,sfield@0x%04x", (""), vdst, ref);
}
void ldvm_OP_SGET_BYTE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s v%d,sfield@0x%04x", (""), vdst, ref);
}
void ldvm_OP_SGET_CHAR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s v%d,sfield@0x%04x", (""), vdst, ref);
}
void ldvm_OP_SGET_SHORT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s v%d,sfield@0x%04x", (""), vdst, ref);
}
void ldvm_OP_SPUT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s v%d,sfield@0x%04x", (""), vdst, ref);
}
void ldvm_OP_SPUT_WIDE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s v%d,sfield@0x%04x", ("-wide"), vdst, ref);
}
void ldvm_OP_SPUT_OBJECT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s v%d,sfield@0x%04x", ("-object"), vdst, ref);
}
void ldvm_OP_SPUT_BOOLEAN(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s v%d,sfield@0x%04x", (""), vdst, ref);
}
void ldvm_OP_SPUT_BYTE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s v%d,sfield@0x%04x", (""), vdst, ref);
}
void ldvm_OP_SPUT_CHAR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s v%d,sfield@0x%04x", (""), vdst, ref);
}
void ldvm_OP_SPUT_SHORT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s v%d,sfield@0x%04x", (""), vdst, ref);
}
void ldvm_OP_INVOKE_VIRTUAL(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-virtual args=%d @0x%04x {regs=0x%04x %x}", vsrc1 >> 4, ref, vdst, vsrc1 & 0x0f);
}
void ldvm_OP_INVOKE_SUPER(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-super args=%d @0x%04x {regs=0x%04x %x}", vsrc1 >> 4, ref, vdst, vsrc1 & 0x0f);
}
void ldvm_OP_INVOKE_DIRECT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-direct args=%d @0x%04x {regs=0x%04x %x}", vsrc1 >> 4, ref, vdst, vsrc1 & 0x0f);
}
void ldvm_OP_INVOKE_STATIC(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-static args=%d @0x%04x {regs=0x%04x %x}", vsrc1 >> 4, ref, vdst, vsrc1 & 0x0f);
}
void ldvm_OP_INVOKE_INTERFACE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-interface args=%d @0x%04x {regs=0x%04x %x}", vsrc1 >> 4, ref, vdst, vsrc1 & 0x0f);
}
void ldvm_OP_UNUSED_73(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw, "ldvm_OP_UNUSED_73 is void.\n");
}
void ldvm_OP_INVOKE_VIRTUAL_RANGE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-virtual-range args=%d @0x%04x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}
void ldvm_OP_INVOKE_SUPER_RANGE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-super-range args=%d @0x%04x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}

void ldvm_OP_INVOKE_DIRECT_RANGE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-direct-range args=%d @0x%04x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}
void ldvm_OP_INVOKE_STATIC_RANGE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-static-range args=%d @0x%04x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}
void ldvm_OP_INVOKE_INTERFACE_RANGE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]); 
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-interface-range args=%d @0x%04x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}
void ldvm_OP_UNUSED_79(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_79 is void.\n");
}
void ldvm_OP_UNUSED_7A(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_7A is void.\n");
}
void ldvm_OP_NEG_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("neg-int"), vdst, vsrc1);
}
void ldvm_OP_NOT_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("not-int"), vdst, vsrc1);
}
void ldvm_OP_NEG_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("neg-long"), vdst, vsrc1);
}
void ldvm_OP_NOT_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("not-long"), vdst, vsrc1);
}
void ldvm_OP_NEG_FLOAT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("neg-float"), vdst, vsrc1);
}
void ldvm_OP_NEG_DOUBLE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("neg-double"), vdst, vsrc1);
}
void ldvm_OP_INT_TO_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("int-to-long"), vdst, vsrc1);
}
void ldvm_OP_INT_TO_FLOAT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("int-to-float"), vdst, vsrc1);
}
void ldvm_OP_INT_TO_DOUBLE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("int-to-double"), vdst, vsrc1);
}
void ldvm_OP_LONG_TO_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("long-to-int"), vdst, vsrc1);
}
void ldvm_OP_LONG_TO_FLOAT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("long-to-float"), vdst, vsrc1);
}
void ldvm_OP_LONG_TO_DOUBLE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("long-to-double"), vdst, vsrc1);
}
void ldvm_OP_FLOAT_TO_INT(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	float val;
	s4 intMin, intMax, result;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("float-to-int"), vdst, vsrc1);
}
void ldvm_OP_FLOAT_TO_LONG(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	float val;
	s4 intMin, intMax, result;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("float-to-long"), vdst, vsrc1);
}
void ldvm_OP_FLOAT_TO_DOUBLE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("float-to-double"), vdst, vsrc1);
}
void ldvm_OP_DOUBLE_TO_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	double val;
	s4 intMin, intMax, result;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("double-to-int"), vdst, vsrc1);
}
void ldvm_OP_DOUBLE_TO_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	double val;
	s4 intMin, intMax, result;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("double-to-long"), vdst, vsrc1);
}
void ldvm_OP_DOUBLE_TO_FLOAT(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s v%d,v%d", ("double-to-float"), vdst, vsrc1);
}
void ldvm_OP_INT_TO_BYTE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|int-to-%s v%d,v%d", ("byte"), vdst, vsrc1);
}
void ldvm_OP_INT_TO_CHAR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|int-to-%s v%d,v%d", ("char"), vdst, vsrc1);
}
void ldvm_OP_INT_TO_SHORT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|int-to-%s v%d,v%d", ("short"), vdst, vsrc1);
}
void ldvm_OP_SUB_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int v%d,v%d", ("sub"), vdst, vsrc1);
}
void ldvm_OP_MUL_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int v%d,v%d", ("mul"), vdst, vsrc1);
}
void ldvm_OP_DIV_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int v%d,v%d", ("div"), vdst, vsrc1);
}
void ldvm_OP_REM_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int v%d,v%d", ("rem"), vdst, vsrc1);
}
void ldvm_OP_AND_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int v%d,v%d", ("and"), vdst, vsrc1);
}
void ldvm_OP_OR_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int v%d,v%d", ("or"), vdst, vsrc1);
}
void ldvm_OP_XOR_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int v%d,v%d", ("xor"), vdst, vsrc1);
}
void ldvm_OP_SHL_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int v%d,v%d", ("shl"), vdst, vsrc1);
}
void ldvm_OP_SHR_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int v%d,v%d", ("shr"), vdst, vsrc1);
}
void ldvm_OP_USHR_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int v%d,v%d", ("ushr"), vdst, vsrc1);
}
void ldvm_OP_ADD_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long v%d,v%d,v%d", ("add"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_SUB_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long v%d,v%d,v%d", ("sub"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_MUL_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long v%d,v%d,v%d", ("mul"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_DIV_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long v%d,v%d,v%d", ("div"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_REM_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long v%d,v%d,v%d", ("rem"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_AND_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long v%d,v%d,v%d", ("and"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_OR_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long v%d,v%d,v%d", ("or"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_XOR_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long v%d,v%d,v%d", ("xor"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_SHL_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long v%d,v%d,v%d", ("shl"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_SHR_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long v%d,v%d,v%d", ("shr"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_USHR_LONG(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long v%d,v%d,v%d", ("ushr"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_ADD_FLOAT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-float v%d,v%d,v%d", ("add"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_SUB_FLOAT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-float v%d,v%d,v%d", ("sub"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_MUL_FLOAT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-float v%d,v%d,v%d", ("mul"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_DIV_FLOAT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-float v%d,v%d,v%d", ("div"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_REM_FLOAT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-float v%d,v%d,v%d", "mod", vdst, vsrc1, vsrc2);
}
void ldvm_OP_ADD_DOUBLE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-double v%d,v%d,v%d", ("add"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_SUB_DOUBLE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-double v%d,v%d,v%d", ("sub"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_MUL_DOUBLE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-double v%d,v%d,v%d", ("mul"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_DIV_DOUBLE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-double v%d,v%d,v%d", ("div"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_REM_DOUBLE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 srcRegs;
	vdst = (inst >> 8);
	srcRegs = (lo.inst[1]);
	vsrc1 = srcRegs & 0xff;
	vsrc2 = srcRegs >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-double v%d,v%d,v%d", "mod", vdst, vsrc1, vsrc2);
}
void ldvm_OP_ADD_INT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int-2addr v%d,v%d", ("add"), vdst, vsrc1);
}
void ldvm_OP_SUB_INT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int-2addr v%d,v%d", ("sub"), vdst, vsrc1);
}
void ldvm_OP_MUL_INT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int-2addr v%d,v%d", ("mul"), vdst, vsrc1);
}
void ldvm_OP_DIV_INT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int-2addr v%d,v%d", ("div"), vdst, vsrc1);
}
void ldvm_OP_REM_INT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int-2addr v%d,v%d", ("rem"), vdst, vsrc1);
}
void ldvm_OP_AND_INT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int-2addr v%d,v%d", ("and"), vdst, vsrc1);
}
void ldvm_OP_OR_INT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int-2addr v%d,v%d", ("or"), vdst, vsrc1);
}
void ldvm_OP_XOR_INT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int-2addr v%d,v%d", ("xor"), vdst, vsrc1);
}
void ldvm_OP_SHL_INT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int-2addr v%d,v%d", ("shl"), vdst, vsrc1);
}
void ldvm_OP_SHR_INT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int-2addr v%d,v%d", ("shr"), vdst, vsrc1);
}
void ldvm_OP_USHR_INT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int-2addr v%d,v%d", ("ushr"), vdst, vsrc1);
}
void ldvm_OP_ADD_LONG_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long-2addr v%d,v%d", ("add"), vdst, vsrc1);
}
void ldvm_OP_SUB_LONG_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long-2addr v%d,v%d", ("sub"), vdst, vsrc1);
}
void ldvm_OP_MUL_LONG_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long-2addr v%d,v%d", ("mul"), vdst, vsrc1);
}
void ldvm_OP_DIV_LONG_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long-2addr v%d,v%d", ("div"), vdst, vsrc1);
}
void ldvm_OP_REM_LONG_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long-2addr v%d,v%d", ("rem"), vdst, vsrc1);
}
void ldvm_OP_AND_LONG_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long-2addr v%d,v%d", ("and"), vdst, vsrc1);
}
void ldvm_OP_OR_LONG_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long-2addr v%d,v%d", ("or"), vdst, vsrc1);
}
void ldvm_OP_XOR_LONG_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long-2addr v%d,v%d", ("xor"), vdst, vsrc1);
}
void ldvm_OP_SHL_LONG_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long-2addr v%d,v%d", ("shl"), vdst, vsrc1);
}
void ldvm_OP_SHR_LONG_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long-2addr v%d,v%d", ("shr"), vdst, vsrc1);
}
void ldvm_OP_USHR_LONG_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-long-2addr v%d,v%d", ("ushr"), vdst, vsrc1);
}
void ldvm_OP_ADD_FLOAT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-float-2addr v%d,v%d", ("add"), vdst, vsrc1);
}
void ldvm_OP_SUB_FLOAT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-float-2addr v%d,v%d", ("sub"), vdst, vsrc1);
}
void ldvm_OP_MUL_FLOAT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-float-2addr v%d,v%d", ("mul"), vdst, vsrc1);
}
void ldvm_OP_DIV_FLOAT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-float-2addr v%d,v%d", ("div"), vdst, vsrc1);
}
void ldvm_OP_REM_FLOAT_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-float-2addr v%d,v%d", "mod", vdst, vsrc1);
}
void ldvm_OP_ADD_DOUBLE_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-double-2addr v%d,v%d", ("add"), vdst, vsrc1);
}
void ldvm_OP_SUB_DOUBLE_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-double-2addr v%d,v%d", ("sub"), vdst, vsrc1);
}
void ldvm_OP_MUL_DOUBLE_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-double-2addr v%d,v%d", ("mul"), vdst, vsrc1);
}
void ldvm_OP_DIV_DOUBLE_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-double-2addr v%d,v%d", ("div"), vdst, vsrc1);
}
void ldvm_OP_REM_DOUBLE_2ADDR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-double-2addr v%d,v%d", "mod", vdst, vsrc1);
}
void ldvm_OP_ADD_INT_LIT16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	vsrc2 = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit16 v%d,v%d,#+0x%04x", ("add"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_RSUB_INT(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	vsrc2 = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|rsub-int v%d,v%d,#+0x%04x", vdst, vsrc1, vsrc2);
}
void ldvm_OP_MUL_INT_LIT16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	vsrc2 = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit16 v%d,v%d,#+0x%04x", ("mul"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_DIV_INT_LIT16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	vsrc2 = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit16 v%d,v%d,#+0x%04x", ("div"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_REM_INT_LIT16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	vsrc2 = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit16 v%d,v%d,#+0x%04x", ("rem"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_AND_INT_LIT16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	vsrc2 = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit16 v%d,v%d,#+0x%04x", ("and"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_OR_INT_LIT16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	vsrc2 = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit16 v%d,v%d,#+0x%04x", ("or"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_XOR_INT_LIT16(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	vsrc2 = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit16 v%d,v%d,#+0x%04x", ("xor"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_ADD_INT_LIT8(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 litInfo;
	vdst = (inst >> 8);
	litInfo = (lo.inst[1]);
	vsrc1 = litInfo & 0xff;
	vsrc2 = litInfo >> 8;
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit8 v%d,v%d,#+0x%02x", ("add"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_RSUB_INT_LIT8(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 litInfo;
	vdst = (inst >> 8);
	litInfo = (lo.inst[1]);
	vsrc1 = litInfo & 0xff;
	vsrc2 = litInfo >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit8 v%d,v%d,#+0x%02x", "rsub", vdst, vsrc1, vsrc2);
}
void ldvm_OP_MUL_INT_LIT8(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 litInfo;
	vdst = (inst >> 8);
	litInfo = (lo.inst[1]);
	vsrc1 = litInfo & 0xff;
	vsrc2 = litInfo >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit8 v%d,v%d,#+0x%02x", ("mul"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_DIV_INT_LIT8(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 litInfo;
	vdst = (inst >> 8);
	litInfo = (lo.inst[1]);
	vsrc1 = litInfo & 0xff;
	vsrc2 = litInfo >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit8 v%d,v%d,#+0x%02x", ("div"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_REM_INT_LIT8(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 litInfo;
	vdst = (inst >> 8);
	litInfo = (lo.inst[1]);
	vsrc1 = litInfo & 0xff;
	vsrc2 = litInfo >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit8 v%d,v%d,#+0x%02x", ("rem"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_AND_INT_LIT8(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 litInfo;
	vdst = (inst >> 8);
	litInfo = (lo.inst[1]);
	vsrc1 = litInfo & 0xff;
	vsrc2 = litInfo >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit8 v%d,v%d,#+0x%02x", ("and"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_OR_INT_LIT8(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 litInfo;
	vdst = (inst >> 8);
	litInfo = (lo.inst[1]);
	vsrc1 = litInfo & 0xff;
	vsrc2 = litInfo >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit8 v%d,v%d,#+0x%02x", ("or"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_XOR_INT_LIT8(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 litInfo;
	vdst = (inst >> 8);
	litInfo = (lo.inst[1]);
	vsrc1 = litInfo & 0xff;
	vsrc2 = litInfo >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit8 v%d,v%d,#+0x%02x", ("xor"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_SHL_INT_LIT8(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 litInfo;
	vdst = (inst >> 8);
	litInfo = (lo.inst[1]);
	vsrc1 = litInfo & 0xff;
	vsrc2 = litInfo >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit8 v%d,v%d,#+0x%02x", ("shl"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_SHR_INT_LIT8(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 litInfo;
	vdst = (inst >> 8);
	litInfo = (lo.inst[1]);
	vsrc1 = litInfo & 0xff;
	vsrc2 = litInfo >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit8 v%d,v%d,#+0x%02x", ("shr"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_USHR_INT_LIT8(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	u2 litInfo;
	vdst = (inst >> 8);
	litInfo = (lo.inst[1]);
	vsrc1 = litInfo & 0xff;
	vsrc2 = litInfo >> 8;

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|%s-int/lit8 v%d,v%d,#+0x%02x", ("ushr"), vdst, vsrc1, vsrc2);
}
void ldvm_OP_IGET_VOLATILE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[(1)]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s v%d,v%d,field@0x%04x", ("-volatile"), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_VOLATILE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[(1)]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s v%d,v%d,field@0x%04x", ("-volatile"), vdst, vsrc1, ref);
}
void ldvm_OP_SGET_VOLATILE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s v%d,sfield@0x%04x", ("-volatile"), vdst, ref);
}
void ldvm_OP_SPUT_VOLATILE(const Loccs_opcode lo, FILE* fpw){	   
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s v%d,sfield@0x%04x", ("-volatile"), vdst, ref);
}
void ldvm_OP_IGET_OBJECT_VOLATILE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s v%d,v%d,field@0x%04x", ("-object-volatile"), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_WIDE_VOLATILE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s v%d,v%d,field@0x%04x", ("-wide-volatile"), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_WIDE_VOLATILE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (getReg(lo, vsrc1));

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s v%d,v%d,field@0x%04x", ("-wide-volatile"), vdst, vsrc1, ref);
}
void ldvm_OP_SGET_WIDE_VOLATILE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s v%d,sfield@0x%04x", ("-wide-volatile"), vdst, ref);
}
void ldvm_OP_SPUT_WIDE_VOLATILE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s v%d,sfield@0x%04x", ("-wide-volatile"), vdst, ref);
}
void ldvm_OP_BREAKPOINT(const Loccs_opcode lo, FILE* fpw){

	//u4 ref;
	//u2 vsrc1, vsrc2, vdst;
	//u2 inst = (lo.inst[0]);
	//u1 originalOpcode = dvmGetOriginalOpcode(lo.inst);
	//LOGV("+++ break 0x%02x (0x%04x -> 0x%04x)", originalOpcode, inst,(((inst) & 0xff00) | originalOpcode));

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw, "ldvm_OP_BREAKPOINT is void.\n");
}
void ldvm_OP_THROW_VERIFICATION_ERROR(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vsrc1 = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw, "ldvm_OP_THROW_VERIFICATION_ERROR is void.\n");
}
void ldvm_OP_EXECUTE_INLINE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	vdst = (lo.inst[(2)]);

	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw, "ldvm_OP_EXECUTE_INLINE is void.\n");
}
void ldvm_OP_EXECUTE_INLINE_RANGE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//(SAVEAREA_FROM_FP(lo.reg)->xtra.currentPc = lo.inst);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	vdst = (lo.inst[(2)]);

	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|execute-inline args=%d @%d {regs=0x%04x}", vsrc1, ref, vdst);
}
void ldvm_OP_INVOKE_OBJECT_INIT_RANGE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//Object* obj;
	vsrc1 = (lo.inst[(2)]);
	//obj = ((Object*) self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x\n",lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw, "ldvm_OP_INVOKE_OBJECT_INIT_RANGE is void.\n");
}
void ldvm_OP_RETURN_VOID_BARRIER(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//Object* obj;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (self.interpSave.curFrame[vsrc1]);
	
	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|return-void");
}
void ldvm_OP_IGET_QUICK(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//Object* obj;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s-quick v%d,v%d,field@+%u", (""), vdst, vsrc1, ref);
}


void ldvm_OP_IGET_WIDE_QUICK(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//Object* obj;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s-quick v%d,v%d,field@+%u", ("-wide"), vdst, vsrc1, ref);
}

void ldvm_OP_IGET_OBJECT_QUICK(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//Object* obj;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s-quick v%d,v%d,field@+%u", ("-object"), vdst, vsrc1, ref);
}

void ldvm_OP_IPUT_QUICK(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//Object* obj;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s-quick v%d,v%d,field@0x%04x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_WIDE_QUICK(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//Object* obj;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s-quick v%d,v%d,field@0x%04x", ("-wide"), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_OBJECT_QUICK(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//Object* obj;
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s-quick v%d,v%d,field@0x%04x", ("-object"), vdst, vsrc1, ref);
}
void ldvm_OP_INVOKE_VIRTUAL_QUICK(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-virtual-quick args=%d @0x%04x {regs=0x%04x %x}", vsrc1 >> 4, ref, vdst, vsrc1 & 0x0f);
}
void ldvm_OP_INVOKE_VIRTUAL_QUICK_RANGE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-virtual-quick-range args=%d @0x%04x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}
void ldvm_OP_INVOKE_SUPER_QUICK(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-super-quick args=%d @0x%04x {regs=0x%04x %x}", vsrc1 >> 4, ref, vdst, vsrc1 & 0x0f);
}
void ldvm_OP_INVOKE_SUPER_QUICK_RANGE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	vsrc1 = ((inst) >> 8);
	ref = (lo.inst[1]);
	vdst = (lo.inst[2]);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-super-quick-range args=%d @0x%04x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}
void ldvm_OP_IPUT_OBJECT_VOLATILE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//InstField* ifield;
	//Object* obj;
	//(SAVEAREA_FROM_FP(self.interpSave.curFrame)->xtra.currentPc = lo.inst);
	vdst = ((inst >> 8) & 0x0f);
	vsrc1 = ((inst) >> 12);
	ref = (lo.inst[1]);
	//obj = (Object*) (self.interpSave.curFrame[vsrc1]);
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw, "ldvm_OP_IPUT_OBJECT_VOLATILE is void.\n");
}
void ldvm_OP_SGET_OBJECT_VOLATILE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);
	
	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s v%d,sfield@0x%04x", ("-object-volatile"), vdst, ref);
}
void ldvm_OP_SPUT_OBJECT_VOLATILE(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	vdst = (inst >> 8);
	ref = (lo.inst[1]);

	fprintf(fpw, "inst = %04x%04x\n",lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s v%d,sfield@0x%04x", ("-object-volatile"), vdst, ref);
}
void ldvm_OP_DISPATCH_FF(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw, "ldvm_OP_DISPATCH_FF is void.\n");
}
void ldvm_OP_CONST_CLASS_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|const-class/jumbo v%d class@0x%08x", vdst, ref);
}
void ldvm_OP_CHECK_CAST_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	//(SAVEAREA_FROM_FP(self.interpSave.curFrame)->xtra.currentPc = lo.inst);
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vsrc1 = (lo.inst[(3)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);
	
	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|check-cast/jumbo v%d,class@0x%08x", vsrc1, ref);
}
void ldvm_OP_INSTANCE_OF_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);
	
	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|instance-of/jumbo v%d,v%d,class@0x%08x", vdst, vsrc1, ref);
}
void ldvm_OP_NEW_INSTANCE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* newObj;
	//(SAVEAREA_FROM_FP(self.interpSave.curFrame)->xtra.currentPc = lo.inst);
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|new-instance/jumbo v%d,class@0x%08x", vdst, ref);
}
void ldvm_OP_NEW_ARRAY_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|new-array/jumbo v%d,v%d,class@0x%08x  (%d elements)", vdst, vsrc1, ref, (s4) (getReg(lo, vsrc1)));
}
void ldvm_OP_FILLED_NEW_ARRAY_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;

	ref = (lo.inst[(1)]) | (u4)(lo.inst[(2)]) << 16;
	vsrc1 = (lo.inst[(3)]);
	vdst = (lo.inst[(4)]);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|filled-new-array/jumbo args=%d @0x%08x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}
void ldvm_OP_IGET_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s/jumbo v%d,v%d,field@0x%08x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_WIDE_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s/jumbo v%d,v%d,field@0x%08x", ("-wide"), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_OBJECT_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s/jumbo v%d,v%d,field@0x%08x", ("-object"), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_BOOLEAN_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s/jumbo v%d,v%d,field@0x%08x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_BYTE_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s/jumbo v%d,v%d,field@0x%08x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_CHAR_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s/jumbo v%d,v%d,field@0x%08x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IGET_SHORT_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iget%s/jumbo v%d,v%d,field@0x%08x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s/jumbo v%d,v%d,field@0x%08x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_WIDE_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s/jumbo v%d,v%d,field@0x%08x", ("-wide"), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_OBJECT_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s/jumbo v%d,v%d,field@0x%08x", ("-object"), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_BOOLEAN_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s/jumbo v%d,v%d,field@0x%08x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_BYTE_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s/jumbo v%d,v%d,field@0x%08x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_CHAR_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s/jumbo v%d,v%d,field@0x%08x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_IPUT_SHORT_JUMBO(const Loccs_opcode lo, FILE* fpw){

	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x%04x%04x%04x%04x\n",lo.inst[4],lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|iput%s/jumbo v%d,v%d,field@0x%08x", (""), vdst, vsrc1, ref);
}
void ldvm_OP_SGET_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	
	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s/jumbo v%d,sfield@0x%08x", (""), vdst, ref);
}
void ldvm_OP_SGET_WIDE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s/jumbo v%d,sfield@0x%08x", ("-wide"), vdst, ref);
}
void ldvm_OP_SGET_OBJECT_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s/jumbo v%d,sfield@0x%08x", ("-object"), vdst, ref);
}
void ldvm_OP_SGET_BOOLEAN_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s/jumbo v%d,sfield@0x%08x", (""), vdst, ref);
}
void ldvm_OP_SGET_BYTE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s/jumbo v%d,sfield@0x%08x", (""), vdst, ref);
}
void ldvm_OP_SGET_CHAR_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s/jumbo v%d,sfield@0x%08x", (""), vdst, ref);
}
void ldvm_OP_SGET_SHORT_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sget%s/jumbo v%d,sfield@0x%08x", (""), vdst, ref);
}
void ldvm_OP_SPUT_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s/jumbo v%d,sfield@0x%08x", (""), vdst, ref);
}
void ldvm_OP_SPUT_WIDE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s/jumbo v%d,sfield@0x%08x", ("-wide"), vdst, ref);
}
void ldvm_OP_SPUT_OBJECT_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s/jumbo v%d,sfield@0x%08x", ("-object"), vdst, ref);
}
void ldvm_OP_SPUT_BOOLEAN_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s/jumbo v%d,sfield@0x%08x", (""), vdst, ref);
}
void ldvm_OP_SPUT_BYTE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s/jumbo v%d,sfield@0x%08x", (""), vdst, ref);
}
void ldvm_OP_SPUT_CHAR_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s/jumbo v%d,sfield@0x%08x", (""), vdst, ref);
}
void ldvm_OP_SPUT_SHORT_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//StaticField* sfield;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);

	fprintf(fpw, "inst = %04x%04x%04x%04x\n",lo.inst[3],lo.inst[2],lo.inst[1],lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|sput%s/jumbo v%d,sfield@0x%08x", (""), vdst, ref);
}
void ldvm_OP_INVOKE_VIRTUAL_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-virtual/jumbo args=%d @0x%08x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}
void ldvm_OP_INVOKE_SUPER_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-super/jumbo args=%d @0x%08x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}
void ldvm_OP_INVOKE_DIRECT_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-direct/jumbo args=%d @0x%08x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}
void ldvm_OP_INVOKE_STATIC_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-static/jumbo args=%d @0x%08x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}

void ldvm_OP_INVOKE_INTERFACE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	u4 ref;
	u2 vsrc1, vsrc2, vdst;
	u2 inst = (lo.inst[0]);
	//ClassObject* clazz;
	//Object* obj;
	ref = (lo.inst[1]) | (u4)(lo.inst[(2)]) << 16;
	vdst = (lo.inst[(3)]);
	vsrc1 = (lo.inst[(4)]);
	//obj = (Object*)(self.interpSave.curFrame[vsrc1]);

	fprintf(fpw, "inst = %04x\n",lo.inst[0]);
	fprintf(fpw,"Inst_Parse:|invoke-interface/jumbo args=%d @0x%08x {regs=v%d-v%d}", vsrc1, ref, vdst, vdst+vsrc1-1);
}
void ldvm_OP_UNUSED_27FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_27FF is void.\n");
}
void ldvm_OP_UNUSED_28FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_28FF is void.\n");
}
void ldvm_OP_UNUSED_29FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_29FF is void.\n");
}
void ldvm_OP_UNUSED_2AFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_2AFF is void.\n");
}
void ldvm_OP_UNUSED_2BFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_2BFF is void.\n");
}
void ldvm_OP_UNUSED_2CFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_2CFF is void.\n");
}
void ldvm_OP_UNUSED_2DFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_2DFF is void.\n");
}
void ldvm_OP_UNUSED_2EFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_2EFF is void.\n");
}
void ldvm_OP_UNUSED_2FFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_2FFF is void.\n");
}
void ldvm_OP_UNUSED_30FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_30FF is void.\n");
}
void ldvm_OP_UNUSED_31FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_31FF is void.\n");
}
void ldvm_OP_UNUSED_32FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_32FF is void.\n");
}
void ldvm_OP_UNUSED_33FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_33FF is void.\n");
}
void ldvm_OP_UNUSED_34FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_34FF is void.\n");
}
void ldvm_OP_UNUSED_35FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_35FF is void.\n");
}
void ldvm_OP_UNUSED_36FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_36FF is void.\n");
}
void ldvm_OP_UNUSED_37FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_37FF is void.\n");
}
void ldvm_OP_UNUSED_38FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_38FF is void.\n");
}
void ldvm_OP_UNUSED_39FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_39FF is void.\n");
}
void ldvm_OP_UNUSED_3AFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_3AFF is void.\n");
}
void ldvm_OP_UNUSED_3BFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_3BFF is void.\n");
}
void ldvm_OP_UNUSED_3CFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_3CFF is void.\n");
}
void ldvm_OP_UNUSED_3DFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_3DFF is void.\n");
}
void ldvm_OP_UNUSED_3EFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_3EFF is void.\n");
}
void ldvm_OP_UNUSED_3FFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_3FFF is void.\n");
}
void ldvm_OP_UNUSED_40FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_40FF is void.\n");
}
void ldvm_OP_UNUSED_41FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_41FF is void.\n");
}
void ldvm_OP_UNUSED_42FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_42FF is void.\n");
}
void ldvm_OP_UNUSED_43FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_43FF is void.\n");
}
void ldvm_OP_UNUSED_44FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_44FF is void.\n");
}
void ldvm_OP_UNUSED_45FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_45FF is void.\n");
}
void ldvm_OP_UNUSED_46FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_46FF is void.\n");
}
void ldvm_OP_UNUSED_47FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_47FF is void.\n");
}
void ldvm_OP_UNUSED_48FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_48FF is void.\n");
}
void ldvm_OP_UNUSED_49FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_49FF is void.\n");
}
void ldvm_OP_UNUSED_4AFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_4AFF is void.\n");
}
void ldvm_OP_UNUSED_4BFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_4BFF is void.\n");
}
void ldvm_OP_UNUSED_4CFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_4CFF is void.\n");
}
void ldvm_OP_UNUSED_4DFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_4DFF is void.\n");
}
void ldvm_OP_UNUSED_4EFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_4EFF is void.\n");
}
void ldvm_OP_UNUSED_4FFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_4FFF is void.\n");
}
void ldvm_OP_UNUSED_50FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_50FF is void.\n");
}
void ldvm_OP_UNUSED_51FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_51FF is void.\n");
}
void ldvm_OP_UNUSED_52FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_52FF is void.\n");
}
void ldvm_OP_UNUSED_53FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_53FF is void.\n");
}
void ldvm_OP_UNUSED_54FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_54FF is void.\n");
}
void ldvm_OP_UNUSED_55FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_55FF is void.\n");
}
void ldvm_OP_UNUSED_56FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_56FF is void.\n");
}
void ldvm_OP_UNUSED_57FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_57FF is void.\n");
}
void ldvm_OP_UNUSED_58FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_58FF is void.\n");
}
void ldvm_OP_UNUSED_59FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_59FF is void.\n");
}
void ldvm_OP_UNUSED_5AFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_5AFF is void.\n");
}
void ldvm_OP_UNUSED_5BFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_5BFF is void.\n");
}
void ldvm_OP_UNUSED_5CFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_5CFF is void.\n");
}
void ldvm_OP_UNUSED_5DFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_5DFF is void.\n");
}
void ldvm_OP_UNUSED_5EFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_5EFF is void.\n");
}
void ldvm_OP_UNUSED_5FFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_5FFF is void.\n");
}
void ldvm_OP_UNUSED_60FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_60FF is void.\n");
}
void ldvm_OP_UNUSED_61FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_61FF is void.\n");
}
void ldvm_OP_UNUSED_62FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_62FF is void.\n");
}
void ldvm_OP_UNUSED_63FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_63FF is void.\n");
}
void ldvm_OP_UNUSED_64FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_64FF is void.\n");
}
void ldvm_OP_UNUSED_65FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_65FF is void.\n");
}
void ldvm_OP_UNUSED_66FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_66FF is void.\n");
}
void ldvm_OP_UNUSED_67FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_67FF is void.\n");
}
void ldvm_OP_UNUSED_68FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_68FF is void.\n");
}
void ldvm_OP_UNUSED_69FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_69FF is void.\n");
}
void ldvm_OP_UNUSED_6AFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_6AFF is void.\n");
}
void ldvm_OP_UNUSED_6BFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_6BFF is void.\n");
}
void ldvm_OP_UNUSED_6CFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_6CFF is void.\n");
}
void ldvm_OP_UNUSED_6DFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_6DFF is void.\n");
}
void ldvm_OP_UNUSED_6EFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_6EFF is void.\n");
}
void ldvm_OP_UNUSED_6FFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_6FFF is void.\n");
}
void ldvm_OP_UNUSED_70FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_70FF is void.\n");
}
void ldvm_OP_UNUSED_71FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_71FF is void.\n");
}
void ldvm_OP_UNUSED_72FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_72FF is void.\n");
}
void ldvm_OP_UNUSED_73FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_73FF is void.\n");
}
void ldvm_OP_UNUSED_74FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_74FF is void.\n");
}
void ldvm_OP_UNUSED_75FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_75FF is void.\n");
}
void ldvm_OP_UNUSED_76FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_76FF is void.\n");
}
void ldvm_OP_UNUSED_77FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_77FF is void.\n");
}
void ldvm_OP_UNUSED_78FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_78FF is void.\n");
}
void ldvm_OP_UNUSED_79FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_79FF is void.\n");
}
void ldvm_OP_UNUSED_7AFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_7AFF is void.\n");
}
void ldvm_OP_UNUSED_7BFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_7BFF is void.\n");
}
void ldvm_OP_UNUSED_7CFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_7CFF is void.\n");
}
void ldvm_OP_UNUSED_7DFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_7DFF is void.\n");
}
void ldvm_OP_UNUSED_7EFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_7EFF is void.\n");
}
void ldvm_OP_UNUSED_7FFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_7FFF is void.\n");
}
void ldvm_OP_UNUSED_80FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_80FF is void.\n");
}
void ldvm_OP_UNUSED_81FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_81FF is void.\n");
}
void ldvm_OP_UNUSED_82FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_82FF is void.\n");
}
void ldvm_OP_UNUSED_83FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_83FF is void.\n");
}
void ldvm_OP_UNUSED_84FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_84FF is void.\n");
}
void ldvm_OP_UNUSED_85FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_85FF is void.\n");
}
void ldvm_OP_UNUSED_86FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_86FF is void.\n");
}
void ldvm_OP_UNUSED_87FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_87FF is void.\n");
}
void ldvm_OP_UNUSED_88FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_88FF is void.\n");
}
void ldvm_OP_UNUSED_89FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_89FF is void.\n");
}
void ldvm_OP_UNUSED_8AFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_8AFF is void.\n");
}
void ldvm_OP_UNUSED_8BFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_8BFF is void.\n");
}
void ldvm_OP_UNUSED_8CFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_8CFF is void.\n");
}
void ldvm_OP_UNUSED_8DFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_8DFF is void.\n");
}
void ldvm_OP_UNUSED_8EFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_8EFF is void.\n");
}
void ldvm_OP_UNUSED_8FFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_8FFF is void.\n");
}
void ldvm_OP_UNUSED_90FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_90FF is void.\n");
}
void ldvm_OP_UNUSED_91FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_91FF is void.\n");
}
void ldvm_OP_UNUSED_92FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_92FF is void.\n");
}
void ldvm_OP_UNUSED_93FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_93FF is void.\n");
}
void ldvm_OP_UNUSED_94FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_94FF is void.\n");
}
void ldvm_OP_UNUSED_95FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_95FF is void.\n");
}
void ldvm_OP_UNUSED_96FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_96FF is void.\n");
}
void ldvm_OP_UNUSED_97FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_97FF is void.\n");
}
void ldvm_OP_UNUSED_98FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_98FF is void.\n");
}
void ldvm_OP_UNUSED_99FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_99FF is void.\n");
}
void ldvm_OP_UNUSED_9AFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_9AFF is void.\n");
}
void ldvm_OP_UNUSED_9BFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_9BFF is void.\n");
}
void ldvm_OP_UNUSED_9CFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_9CFF is void.\n");
}
void ldvm_OP_UNUSED_9DFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_9DFF is void.\n");
}
void ldvm_OP_UNUSED_9EFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_9EFF is void.\n");
}
void ldvm_OP_UNUSED_9FFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_9FFF is void.\n");
}
void ldvm_OP_UNUSED_A0FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_A0FF is void.\n");
}
void ldvm_OP_UNUSED_A1FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_A1FF is void.\n");
}
void ldvm_OP_UNUSED_A2FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_A2FF is void.\n");
}
void ldvm_OP_UNUSED_A3FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_A3FF is void.\n");
}
void ldvm_OP_UNUSED_A4FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_A4FF is void.\n");
}
void ldvm_OP_UNUSED_A5FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_A5FF is void.\n");
}
void ldvm_OP_UNUSED_A6FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_A6FF is void.\n");
}
void ldvm_OP_UNUSED_A7FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_A7FF is void.\n");
}
void ldvm_OP_UNUSED_A8FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_A8FF is void.\n");
}
void ldvm_OP_UNUSED_A9FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_A9FF is void.\n");
}
void ldvm_OP_UNUSED_AAFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_AAFF is void.\n");
}
void ldvm_OP_UNUSED_ABFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_ABFF is void.\n");
}
void ldvm_OP_UNUSED_ACFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_ACFF is void.\n");
}
void ldvm_OP_UNUSED_ADFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_ADFF is void.\n");
}
void ldvm_OP_UNUSED_AEFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_AEFF is void.\n");
}
void ldvm_OP_UNUSED_AFFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_AFFF is void.\n");
}
void ldvm_OP_UNUSED_B0FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_B0FF is void.\n");
}
void ldvm_OP_UNUSED_B1FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_B1FF is void.\n");
}
void ldvm_OP_UNUSED_B2FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_B2FF is void.\n");
}
void ldvm_OP_UNUSED_B3FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_B3FF is void.\n");
}
void ldvm_OP_UNUSED_B4FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_B4FF is void.\n");
}
void ldvm_OP_UNUSED_B5FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_B5FF is void.\n");
}
void ldvm_OP_UNUSED_B6FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_B6FF is void.\n");
}
void ldvm_OP_UNUSED_B7FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_B7FF is void.\n");
}
void ldvm_OP_UNUSED_B8FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_B8FF is void.\n");
}
void ldvm_OP_UNUSED_B9FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_B9FF is void.\n");
}
void ldvm_OP_UNUSED_BAFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_BAFF is void.\n");
}
void ldvm_OP_UNUSED_BBFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_BBFF is void.\n");
}
void ldvm_OP_UNUSED_BCFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_BCFF is void.\n");
}
void ldvm_OP_UNUSED_BDFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_BDFF is void.\n");
}
void ldvm_OP_UNUSED_BEFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_BEFF is void.\n");
}
void ldvm_OP_UNUSED_BFFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_BFFF is void.\n");
}
void ldvm_OP_UNUSED_C0FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_C0FF is void.\n");
}
void ldvm_OP_UNUSED_C1FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_C1FF is void.\n");
}
void ldvm_OP_UNUSED_C2FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_C2FF is void.\n");
}
void ldvm_OP_UNUSED_C3FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_C3FF is void.\n");
}
void ldvm_OP_UNUSED_C4FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_C4FF is void.\n");
}
void ldvm_OP_UNUSED_C5FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_C5FF is void.\n");
}
void ldvm_OP_UNUSED_C6FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_C6FF is void.\n");
}
void ldvm_OP_UNUSED_C7FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_C7FF is void.\n");
}
void ldvm_OP_UNUSED_C8FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_C8FF is void.\n");
}
void ldvm_OP_UNUSED_C9FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_C9FF is void.\n");
}
void ldvm_OP_UNUSED_CAFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_CAFF is void.\n");
}
void ldvm_OP_UNUSED_CBFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_CBFF is void.\n");
}
void ldvm_OP_UNUSED_CCFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_CCFF is void.\n");
}
void ldvm_OP_UNUSED_CDFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_CDFF is void.\n");
}
void ldvm_OP_UNUSED_CEFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_CEFF is void.\n");
}
void ldvm_OP_UNUSED_CFFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_CFFF is void.\n");
}
void ldvm_OP_UNUSED_D0FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_D0FF is void.\n");
}
void ldvm_OP_UNUSED_D1FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_D1FF is void.\n");
}
void ldvm_OP_UNUSED_D2FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_D2FF is void.\n");
}
void ldvm_OP_UNUSED_D3FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_D3FF is void.\n");
}
void ldvm_OP_UNUSED_D4FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_D4FF is void.\n");
}
void ldvm_OP_UNUSED_D5FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_D5FF is void.\n");
}
void ldvm_OP_UNUSED_D6FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_D6FF is void.\n");
}
void ldvm_OP_UNUSED_D7FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_D7FF is void.\n");
}
void ldvm_OP_UNUSED_D8FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_D8FF is void.\n");
}
void ldvm_OP_UNUSED_D9FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_D9FF is void.\n");
}
void ldvm_OP_UNUSED_DAFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_DAFF is void.\n");
}
void ldvm_OP_UNUSED_DBFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_DBFF is void.\n");
}
void ldvm_OP_UNUSED_DCFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_DCFF is void.\n");
}
void ldvm_OP_UNUSED_DDFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_DDFF is void.\n");
}
void ldvm_OP_UNUSED_DEFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_DEFF is void.\n");
}
void ldvm_OP_UNUSED_DFFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_DFFF is void.\n");
}
void ldvm_OP_UNUSED_E0FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_E0FF is void.\n");
}
void ldvm_OP_UNUSED_E1FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_E1FF is void.\n");
}
void ldvm_OP_UNUSED_E2FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_E2FF is void.\n");
}
void ldvm_OP_UNUSED_E3FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_E3FF is void.\n");
}
void ldvm_OP_UNUSED_E4FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_E4FF is void.\n");
}
void ldvm_OP_UNUSED_E5FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_E5FF is void.\n");
}
void ldvm_OP_UNUSED_E6FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_E6FF is void.\n");
}
void ldvm_OP_UNUSED_E7FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_E7FF is void.\n");
}
void ldvm_OP_UNUSED_E8FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_E8FF is void.\n");
}
void ldvm_OP_UNUSED_E9FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_E9FF is void.\n");
}
void ldvm_OP_UNUSED_EAFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_EAFF is void.\n");
}
void ldvm_OP_UNUSED_EBFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_EBFF is void.\n");
}
void ldvm_OP_UNUSED_ECFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_ECFF is void.\n");
}
void ldvm_OP_UNUSED_EDFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_EDFF is void.\n");
}
void ldvm_OP_UNUSED_EEFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_EEFF is void.\n");
}
void ldvm_OP_UNUSED_EFFF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_EFFF is void.\n");
}
void ldvm_OP_UNUSED_F0FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_F0FF is void.\n");
}
void ldvm_OP_UNUSED_F1FF(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_UNUSED_F1FF is void.\n");
}
void ldvm_OP_INVOKE_OBJECT_INIT_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_INVOKE_OBJECT_INIT_JUMBO is void.\n");
}
void ldvm_OP_IGET_VOLATILE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_IGET_VOLATILE_JUMBO is void.\n");
}
void ldvm_OP_IGET_WIDE_VOLATILE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_IGET_WIDE_VOLATILE_JUMBO is void.\n");
}
void ldvm_OP_IGET_OBJECT_VOLATILE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_IGET_OBJECT_VOLATILE_JUMBO is void.\n");
}
void ldvm_OP_IPUT_VOLATILE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_IPUT_VOLATILE_JUMBO is void.\n");
}
void ldvm_OP_IPUT_WIDE_VOLATILE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_IPUT_WIDE_VOLATILE_JUMBO is void.\n");
}
void ldvm_OP_IPUT_OBJECT_VOLATILE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_IPUT_OBJECT_VOLATILE_JUMBO is void.\n");
}
void ldvm_OP_SGET_VOLATILE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_SGET_VOLATILE_JUMBO is void.\n");
}
void ldvm_OP_SGET_WIDE_VOLATILE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_SGET_WIDE_VOLATILE_JUMBO is void.\n");
}
void ldvm_OP_SGET_OBJECT_VOLATILE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_SGET_OBJECT_VOLATILE_JUMBO is void.\n");
}
void ldvm_OP_SPUT_VOLATILE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_SPUT_VOLATILE_JUMBO is void.\n");
}
void ldvm_OP_SPUT_WIDE_VOLATILE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_SPUT_WIDE_VOLATILE_JUMBO is void.\n");
}
void ldvm_OP_SPUT_OBJECT_VOLATILE_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_SPUT_OBJECT_VOLATILE_JUMBO is void.\n");
}
void ldvm_OP_THROW_VERIFICATION_ERROR_JUMBO(const Loccs_opcode lo, FILE* fpw){
	fprintf(fpw, "ldvm_OP_THROW_VERIFICATION_ERROR_JUMBO is void.\n");
}