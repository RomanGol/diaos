#ifndef _TRACE_H_
#define _TRACE_H_

#include <cstdint>
#include <cstdio>
#include <iostream>
#include <iomanip>
	
typedef uint16_t u2;
typedef uint32_t u4;
typedef uint64_t u8;
typedef int8_t s1;
typedef int16_t s2;
typedef int32_t s4;
typedef int64_t s8;


const static uint32_t InstMaxNum = 5; // add by shujunliang
const static uint32_t RegMaxNum = 16;
const static uint32_t ClassNameMaxLen = 128;
const static uint32_t MethodNameMaxLen = 64;
const static uint32_t LogContentMaxLen = 256;

struct Loccs_opcode
{
	uint32_t threadId; // 4 bytes
	uint32_t pc; // 4 bytes
	uint16_t inst[InstMaxNum]; // may be this should be 2 * 4 or 2 * 8 bytes
	uint32_t reg[RegMaxNum]; // 64 bytes
	char methodDescriptor[ClassNameMaxLen]; // 128 bytes
	char methodName[MethodNameMaxLen]; // 64 bytes
};

//以下为暂定的解析后的数据结构，还未投入使用
struct Instrtype
{
	int32_t category;//类型
	uint16_t length;//长度,1~5	 
	uint16_t inst[5];
	unsigned char method_name[MethodNameMaxLen];
	unsigned char class_name[ClassNameMaxLen];
};

struct Argtype
{
	bool used;//是否使用
	
};									  
struct Argref:Argtype{
	u4 value;
};
struct Argoffset:Argtype{
	s4 value;
};
struct ArgReg:Argtype{
	uint8_t num;
	u2 value;
};
struct Opcode_parsed{
	uint32_t pc;
	char description[LogContentMaxLen];
	Instrtype Instruction;
	Argtype vdst;
	Argtype vscr1;
	Argtype vscr2;
	Argtype ref;
	Argtype offset;
};

#endif