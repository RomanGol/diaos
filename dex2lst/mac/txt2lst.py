import re

f = open("dump.txt")
lst = f.readlines()
f.close()

w = open("class.dlist", 'w')

for s in lst:
	if re.search( "Class descriptor", s ):
		t = s.split(':')[1]
		t = t[2 : len(t) - 2]
		w.write(t)
		w.write('\n')

w.close()
