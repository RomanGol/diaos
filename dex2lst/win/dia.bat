@echo off
@set apkFile=%1
@set apkDir=%apkFile:~0,-4%
unzip -q %apkFile% -d %apkDir%
dexdump %apkDir%\\classes.dex > dump.txt
dexdump %apkDir%\\classes.dex | grep "Class descriptor" > %apkDir%.txt
trim %apkDir%.txt class.dlist
rmdir /S /Q %apkDir%
del /Q %apkDir%.txt
